define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_d70bdc2885fd45d5ab4b6f8f08b69e0c: function AS_BarButtonItem_d70bdc2885fd45d5ab4b6f8f08b69e0c(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    AS_BarButtonItem_ha9983a93f564e6689f45b14634bab61: function AS_BarButtonItem_ha9983a93f564e6689f45b14634bab61(eventobject) {
        var self = this;
        this.navigateCustomBack();
    },
    /** postShow defined for frmLoanPayoff **/
    AS_Form_b1684971d7c94f7aa4fd97d0a130cea0: function AS_Form_b1684971d7c94f7aa4fd97d0a130cea0(eventobject) {
        var self = this;
        this.postShow();
    },
    /** init defined for frmLoanPayoff **/
    AS_Form_b86fb24b31bf4b1d8ace1d52b7b224b7: function AS_Form_b86fb24b31bf4b1d8ace1d52b7b224b7(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmLoanPayoff **/
    AS_Form_g7ae4569d6834fb587767707fbacfff6: function AS_Form_g7ae4569d6834fb587767707fbacfff6(eventobject) {
        var self = this;
        this.preShow();
    }
});