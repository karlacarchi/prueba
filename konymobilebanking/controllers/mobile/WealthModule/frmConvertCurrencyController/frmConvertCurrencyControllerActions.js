define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_a57d8703e58745699e5263fe09e577cc: function AS_BarButtonItem_a57d8703e58745699e5263fe09e577cc(eventobject) {
        var self = this;
        this.backOnClick();
    },
    AS_BarButtonItem_f8db3f1d242b40bb892f30098586a2c3: function AS_BarButtonItem_f8db3f1d242b40bb892f30098586a2c3(eventobject) {
        var self = this;
        this.cancelOnClick();
    },
    /** onClick defined for btnEight **/
    AS_Button_a4dfed3b25964acb9cbd5d4ceeb12641: function AS_Button_a4dfed3b25964acb9cbd5d4ceeb12641(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnSix **/
    AS_Button_bc518c2be1d94b94888551fa72bef783: function AS_Button_bc518c2be1d94b94888551fa72bef783(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** onClick defined for btnFive **/
    AS_Button_bef3dc2e333f4c88be57fc5510227061: function AS_Button_bef3dc2e333f4c88be57fc5510227061(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for btnOne **/
    AS_Button_c8caf79e2ad24f608a2d291812df861a: function AS_Button_c8caf79e2ad24f608a2d291812df861a(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnNine **/
    AS_Button_d2a28eee55c14930bb61d191a86fb9f9: function AS_Button_d2a28eee55c14930bb61d191a86fb9f9(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnFour **/
    AS_Button_dc152db74e14455987eece06f930e911: function AS_Button_dc152db74e14455987eece06f930e911(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnTwo **/
    AS_Button_e7379b7e3c2f426ab23c4f68840241ec: function AS_Button_e7379b7e3c2f426ab23c4f68840241ec(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnThree **/
    AS_Button_gb809f9987494b39bfab5e05b28f465f: function AS_Button_gb809f9987494b39bfab5e05b28f465f(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnSeven **/
    AS_Button_i372484f22f84cafa98a7a3819328b38: function AS_Button_i372484f22f84cafa98a7a3819328b38(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnZero **/
    AS_Button_id9572d80f654f7d9d1ee897e4886b8a: function AS_Button_id9572d80f654f7d9d1ee897e4886b8a(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** postShow defined for frmConvertCurrency **/
    AS_Form_abbf621158bb4c968729aaaa46c125bb: function AS_Form_abbf621158bb4c968729aaaa46c125bb(eventobject) {
        var self = this;
        return self.postShow.call(this);
    },
    /** init defined for frmConvertCurrency **/
    AS_Form_d98ba7cd2fe04c49831d1bc5dd100f82: function AS_Form_d98ba7cd2fe04c49831d1bc5dd100f82(eventobject) {
        var self = this;
        return self.init.call(this);
    },
    /** preShow defined for frmConvertCurrency **/
    AS_Form_dac44b67e0634f5dad84261b0489b782: function AS_Form_dac44b67e0634f5dad84261b0489b782(eventobject) {
        var self = this;
        return self.preShow.call(this);
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_j8a433e99c0746c6b0f73e1eed64c79a: function AS_Image_j8a433e99c0746c6b0f73e1eed64c79a(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar()
    }
});