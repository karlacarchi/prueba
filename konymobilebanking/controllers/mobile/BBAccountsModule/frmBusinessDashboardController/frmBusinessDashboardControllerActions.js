define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_b8e6117a48a44297942bef0d3277939f: function AS_BarButtonItem_b8e6117a48a44297942bef0d3277939f(eventobject) {
        var self = this;
        return self.navigateToCombinedSelector.call(this);
    },
    /** onTouchStart defined for flxDummyHorizontalScroll **/
    AS_FlexScrollContainer_bb65e7f72c3e44328bb7aba449650e6e: function AS_FlexScrollContainer_bb65e7f72c3e44328bb7aba449650e6e(eventobject, x, y) {
        var self = this;
        this.bringFlxDashboardHeaderToFront();
    },
    /** postShow defined for frmBusinessDashboard **/
    AS_Form_e622ed9f48a44f8db528c7c156fb5ac0: function AS_Form_e622ed9f48a44f8db528c7c156fb5ac0(eventobject) {
        var self = this;
        this.postShow();
    },
    /** init defined for frmBusinessDashboard **/
    AS_Form_f9c1302f7d314729a5c40ecdc7bd5374: function AS_Form_f9c1302f7d314729a5c40ecdc7bd5374(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmBusinessDashboard **/
    AS_Form_fb991be7614a448bb2fa98b9accb2b54: function AS_Form_fb991be7614a448bb2fa98b9accb2b54(eventobject) {
        var self = this;
        this.preshow();
    },
    /** onDownloadComplete defined for imgAd4 **/
    AS_Image_b83a4fb80df5471a8af8239dbd28c4b9: function AS_Image_b83a4fb80df5471a8af8239dbd28c4b9(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 4);
    },
    /** onDownloadComplete defined for imgAd1 **/
    AS_Image_c86405045aa04dffa72b74da5845da5c: function AS_Image_c86405045aa04dffa72b74da5845da5c(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 1);
    },
    /** onDownloadComplete defined for imgAd3 **/
    AS_Image_d759f3768cf0454a8be054c8b94d931a: function AS_Image_d759f3768cf0454a8be054c8b94d931a(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 3);
    },
    /** onDownloadComplete defined for imgAd2 **/
    AS_Image_e3f2ee4031994b7f8a79ebce90f2e9ac: function AS_Image_e3f2ee4031994b7f8a79ebce90f2e9ac(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 2);
    },
    /** onDownloadComplete defined for imgAd5 **/
    AS_Image_ed61b986e9ac46d1ba58d42cda0f98bc: function AS_Image_ed61b986e9ac46d1ba58d42cda0f98bc(eventobject, imagesrc, issuccess) {
        var self = this;
        this.onAdDownloadComplete(issuccess, 5);
    },
    /** onTouchStart defined for segAccounts **/
    AS_Segment_b383aae6f3774f72951a6d829807b2eb: function AS_Segment_b383aae6f3774f72951a6d829807b2eb(eventobject, x, y) {
        var self = this;
        this.bringFlxDashboardToFront();
    }
});