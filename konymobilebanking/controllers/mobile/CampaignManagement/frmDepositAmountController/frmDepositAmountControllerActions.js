define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_af3ca93f573b4793a18cd651829364f8: function AS_BarButtonItem_af3ca93f573b4793a18cd651829364f8(eventobject) {
        var self = this;
        this.onCancelClick();
    },
    /** onClick defined for btnSeven **/
    AS_Button_a7ba043cc8d74aa0b654e730a3163805: function AS_Button_a7ba043cc8d74aa0b654e730a3163805(eventobject) {
        var self = this;
        this.setKeypadChar(7);
    },
    /** onClick defined for btnFive **/
    AS_Button_aaf3fa43ed2945949f66b6004b510ae5: function AS_Button_aaf3fa43ed2945949f66b6004b510ae5(eventobject) {
        var self = this;
        this.setKeypadChar(5);
    },
    /** onClick defined for btnDot **/
    AS_Button_ad8c1d5211cf457d96c094160176ea4d: function AS_Button_ad8c1d5211cf457d96c094160176ea4d(eventobject) {
        var self = this;
        this.setKeypadChar('.');
    },
    /** onClick defined for btnThree **/
    AS_Button_d613fc79e36247d3b15c0430012210be: function AS_Button_d613fc79e36247d3b15c0430012210be(eventobject) {
        var self = this;
        this.setKeypadChar(3);
    },
    /** onClick defined for btnOne **/
    AS_Button_df936ebdc855496c86b5c2bd26d75c62: function AS_Button_df936ebdc855496c86b5c2bd26d75c62(eventobject) {
        var self = this;
        this.setKeypadChar(1);
    },
    /** onClick defined for btnZero **/
    AS_Button_f9628f91c9404f17afec47a7b2ea7ec3: function AS_Button_f9628f91c9404f17afec47a7b2ea7ec3(eventobject) {
        var self = this;
        this.setKeypadChar(0);
    },
    /** onClick defined for btnNine **/
    AS_Button_g110ed24367b4f68a02699a136c9330b: function AS_Button_g110ed24367b4f68a02699a136c9330b(eventobject) {
        var self = this;
        this.setKeypadChar(9);
    },
    /** onClick defined for btnFour **/
    AS_Button_g1e3f08fd7de4c6ba3490e1730d43dff: function AS_Button_g1e3f08fd7de4c6ba3490e1730d43dff(eventobject) {
        var self = this;
        this.setKeypadChar(4);
    },
    /** onClick defined for btnTwo **/
    AS_Button_ga4f0d0200ed44b3b1d5d676aef3ee6e: function AS_Button_ga4f0d0200ed44b3b1d5d676aef3ee6e(eventobject) {
        var self = this;
        this.setKeypadChar(2);
    },
    /** onClick defined for btnEight **/
    AS_Button_h147c1dd62e8475eaa13d0c6158353d6: function AS_Button_h147c1dd62e8475eaa13d0c6158353d6(eventobject) {
        var self = this;
        this.setKeypadChar(8);
    },
    /** onClick defined for btnSix **/
    AS_Button_i7f435117b734fc0a3244082fc19f4a4: function AS_Button_i7f435117b734fc0a3244082fc19f4a4(eventobject) {
        var self = this;
        this.setKeypadChar(6);
    },
    /** init defined for frmDepositAmount **/
    AS_Form_f39cacb4748a488da0096a9a427b12ca: function AS_Form_f39cacb4748a488da0096a9a427b12ca(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmDepositAmount **/
    AS_Form_f8b39a0bb1ef4022b587f5a48342a515: function AS_Form_f8b39a0bb1ef4022b587f5a48342a515(eventobject) {
        var self = this;
        this.preShow();
    },
    /** onTouchEnd defined for imgClearKeypad **/
    AS_Image_fab486ab37984d0fb268d01b8596c8a1: function AS_Image_fab486ab37984d0fb268d01b8596c8a1(eventobject, x, y) {
        var self = this;
        this.clearKeypadChar();
    }
});