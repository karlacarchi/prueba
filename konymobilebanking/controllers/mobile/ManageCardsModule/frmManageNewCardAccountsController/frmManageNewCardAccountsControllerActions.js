define({
    /*
          This is an auto generated file and any modifications to it may result in corruption of the action sequence.
        */
    AS_BarButtonItem_a98faba41f9c4390b77373e34103fb75: function AS_BarButtonItem_a98faba41f9c4390b77373e34103fb75(eventobject) {
        var self = this;
        this.cancelCommon();
    },
    /** init defined for frmManageNewCardAccounts **/
    AS_Form_c0bdc608c64242ecb32a2cf7ef9718c6: function AS_Form_c0bdc608c64242ecb32a2cf7ef9718c6(eventobject) {
        var self = this;
        this.init();
    },
    /** preShow defined for frmManageNewCardAccounts **/
    AS_Form_e00cd6c92d344a1ab5e6cd2bf13e2e58: function AS_Form_e00cd6c92d344a1ab5e6cd2bf13e2e58(eventobject) {
        var self = this;
        this.preShow();
    },
    /** postShow defined for frmManageNewCardAccounts **/
    AS_Form_ed4977eead3d489ea21fb95f258f39db: function AS_Form_ed4977eead3d489ea21fb95f258f39db(eventobject) {
        var self = this;
        this.postShow();
    },
    /** onRowClick defined for segTransactions **/
    AS_Segment_i5d29c43fce1405abcbbf3e61ce2ae4a: function AS_Segment_i5d29c43fce1405abcbbf3e61ce2ae4a(eventobject, sectionNumber, rowNumber) {
        var self = this;
        this.onRowClick();
    }
});