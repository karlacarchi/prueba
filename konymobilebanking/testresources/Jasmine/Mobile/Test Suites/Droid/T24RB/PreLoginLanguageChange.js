describe("PreLoginLanguageChange", function() {
	beforeEach(async function() {
	
	    var flgLogoutForm = await kony.automation.playback.waitFor(["frmLogout", "btnLogIn"], 3000);
	    kony.print("flgLogoutForm: " + flgLogoutForm);
	
	    if (flgLogoutForm === true || flgLogoutForm === 1) {
	        kony.automation.button.click(["frmLogout", "btnLogIn"]);
	        kony.automation.playback.waitFor(["frmLogin", "login", "tbxUsername"], 10000);
	        await kony.automation.playback.wait(3000);
	        expect(kony.automation.widget.getWidgetProperty(["frmLogin", "login", "tbxPassword"], "text")).toEqual("");
	    }
	
	});
	
	function clickOnPreLogin_SupportBtn(){
	
	  kony.automation.playback.waitFor(["frmLogin","btnSupport"],15000);
	  kony.automation.button.click(["frmLogin","btnSupport"]);
	}
	
	function verifyAppVersion(){
	
	  kony.automation.playback.waitFor(["frmSupport","lblAppVersion"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmSupport","lblAppVersion"], "text")).not.toBe('');
	  kony.automation.playback.waitFor(["frmSupport","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmSupport","customHeader","flxBack"]);
	  kony.automation.playback.wait(10000);
	}
	
	function VerifyPreLogin_FAQLink(){
	
	  kony.automation.playback.waitFor(["frmSupport","segSupport"],15000);
	  kony.automation.segmentedui.click(["frmSupport","segSupport[0,0]"]);
	  kony.automation.playback.waitFor(["frmSupportInfo","customHeader","lblLocateUs"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmSupportInfo","customHeader","lblLocateUs"], "text")).not.toBe('');
	}
	function VerifyPreLogin_TermsConditions(){
	
	  kony.automation.playback.waitFor(["frmSupport","segSupport"],15000);
	  kony.automation.segmentedui.click(["frmSupport","segSupport[0,1]"]);
	  kony.automation.playback.waitFor(["frmSupportInfo","customHeader","lblLocateUs"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmSupportInfo","customHeader","lblLocateUs"], "text")).not.toBe('');
	}
	
	function VerifyPreLogin_PrivacyPolicy(){
	
	  kony.automation.playback.waitFor(["frmSupport","segSupport"],15000);
	  kony.automation.segmentedui.click(["frmSupport","segSupport[0,2]"]);
	  kony.automation.playback.waitFor(["frmSupportInfo","customHeader","lblLocateUs"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmSupportInfo","customHeader","lblLocateUs"], "text")).not.toBe('');
	}
	
	function MoveBackFrom_Support(){
	
	  kony.automation.playback.waitFor(["frmSupportInfo","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmSupportInfo","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmSupport","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmSupport","customHeader","flxBack"]);
	  kony.automation.playback.wait(10000);
	
	}
	
	function changeLanguage_French(){
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"],15000);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,4]"]);
		kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"],15000);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
	}
	
	function changeLanguage_German(){
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"],15000);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,3]"]);
		kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"],15000);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
	}
	
	function changeLanguage_Spanish(){
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"],15000);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,2]"]);
		kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"],15000);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
	}
	
	function changeLanguage_UK_English(){
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"],15000);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,1]"]);
		kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"],15000);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
	}
	
	function changeLanguage_US_English(){
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
		kony.automation.flexcontainer.click(["frmLogin","flxLanguageSelection"]);
		kony.automation.playback.waitFor(["frmLogin","segSelectLanguage"],15000);
		kony.automation.segmentedui.click(["frmLogin","segSelectLanguage[0,0]"]);
		kony.automation.playback.waitFor(["frmLogin","btnupdateLanguage"],15000);
		kony.automation.button.click(["frmLogin","btnupdateLanguage"]);
		kony.automation.alert.click(0);
		kony.automation.playback.waitFor(["frmLogin","flxLanguageSelection"],15000);
	}
	
	function verifyContactUS() {
		kony.automation.playback.waitFor(["frmSupport","segTimings"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmSupport","segTimings[0,0]","lblTimingValue"], "text")).not.toBeNull();
		kony.automation.playback.waitFor(["frmSupport","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmSupport","customHeader","flxBack"]);
	}
	
	it("PreLogin_VerifyLanguage_French", async function() {
	  changeLanguage_French();
	},60000);
	
	it("PreLogin_VerifyLanguage_German", async function() {
	  changeLanguage_German();
	},60000);
	
	it("PreLogin_VerifyLanguage_Spanish", async function() {
	  changeLanguage_Spanish();
	},60000);
	
	it("PreLogin_VerifyLanguage_US_English", async function() {
	  changeLanguage_US_English();
	},60000);
	
	it("PreLogin_VerifyLanguage_UK_English", async function() {
	  changeLanguage_UK_English();
	},60000);
});