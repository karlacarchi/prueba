describe("UnfiedTransfersFlow", function() {
	beforeEach(async function() {
	// 	jasmine.DEFAULT_TIMEOUT_INTERVAL = 90000;
	    var flgLoginForm = await kony.automation.playback.waitFor(["frmLogin", "login", "btnLogIn"], 2000);
	    var flgLogoutForm = await kony.automation.playback.waitFor(["frmLogout", "btnLogIn"], 2000);
	    kony.print("flgLoginForm: " + flgLoginForm + " ,flgLogoutForm: " + flgLogoutForm);
	
	    if (flgLogoutForm) {
	        kony.automation.button.click(["frmLogout", "btnLogIn"]);
	        kony.automation.playback.waitFor(["frmLogin", "login", "tbxUsername"], 10000);
	        await kony.automation.playback.wait(3000);
	//         expect(kony.automation.widget.getWidgetProperty(["frmLogin", "login", "tbxPassword"], "text")).toEqual("");
	        await login(LoginDetails.username);
	    } else if (flgLoginForm === true || flgLoginForm === 1) {
	        await login(LoginDetails.username);
	    }
	},12000);
	
	
	async function login(username) {
	    await kony.automation.playback.waitFor(["frmLogin", "login", "tbxUsername"]);
	    kony.automation.textbox.enterText(["frmLogin", "login", "tbxUsername"], username);
	    kony.automation.textbox.enterText(["frmLogin", "login", "tbxPassword"], LoginDetails.password);
	    await kony.automation.playback.waitFor(["frmLogin", "login", "btnLogIn"]);
	    kony.automation.button.click(["frmLogin", "login", "btnLogIn"]);
	    //Verifying Terms and Condition page -
	    var frmTnC = await kony.automation.playback.waitFor(["frmTermsAndCondition", "flxCheckBox"], 20000);
	    if (frmTnC) {
	        kony.automation.flexcontainer.click(["frmTermsAndCondition", "flxCheckBox"]);
	        await kony.automation.playback.waitFor(["frmTermsAndCondition", "btnContinue"]);
	        kony.automation.button.click(["frmTermsAndCondition", "btnContinue"]);
	    }
	
	    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "lblBankName"], 15000);
	}
	
	function SelectAccountOndashBoard(account){
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","segAccounts"],15000);
	  let indices = getIndex(account);
	  kony.automation.widget.touch(["frmUnifiedDashboard","segAccounts"], null,null,[80,97]);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","segAccounts[" + indices.i + "," + indices.j +"]"]);
	  kony.automation.playback.wait(10000);
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","lblAccountName"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountSummaryNative","lblAccountName"], "text")).not.toBe('');
	
	}
	
	// function getIndex(account){
	//   let segData = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","segAccounts"], "data");
	//   let i=0;
	//   let j=0;
	
	//   for(var a = 0; a<segData.length; a++){
	//     for(var b=0; b<segData[a][1].length; b++){
	//       if(segData[a][1][b].accountName.includes(account)){
	//         i=a;
	//         j=b;
	//         break;
	//       }
	//     }
	//   }
	
	//   return {
	//     i,
	//     j
	//   };
	// }
	
	 function getIndex(account){
	  let segData = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","segAccounts"], "data");
	  let i=0;
	  let j=0;
	
	  for(var a = 0; a<segData.length; a++){
	    for(var b=0; b<segData[a][1].length; b++){
	      if(segData[a][1][b].accountName === undefined){
	        kony.automation.flexcontainer.click(["frmUnifiedDashboard","segAccounts[" + a + "," + b +"]","flxViewContainer"]);
	        kony.automation.playback.wait(2000);
	        return getIndex(account);                                   
	
	      }else{
	        if(segData[a][1][b].accountName.includes(account)){
	          i=a;
	          j=b;
	          break;
	        }                               
	      }
	    }
	  }
	  return {
	    i,
	    j
	  };
	}
	
	function ClickonFirstSavingAccount(){
	
	  SelectAccountOndashBoard(Transfers.savingsAccount.name);
	
	}
	
	function ClickonFirstCheckingAccount(){
	
	  SelectAccountOndashBoard(Transfers.checkingAccount.name);
	}
	
	function ClickonFirstLoanAccount(){
	
	  SelectAccountOndashBoard(Transfers.loanAccount.name);
	}
	
	function ClickonFirstCreditCardAccount(){
	
	  SelectAccountOndashBoard("Credit Card");
	}
	
	function ClickonFirstDepositAccount(){
	
	  SelectAccountOndashBoard(Transfers.depositAccount.name);
	}
	
	function initiateAndCancelTransfer(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
		kony.automation.playback.waitFor(["frmEuropeTransferToAccount","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEuropeTransferToAccount","customHeader","flxBack"]);
	}
	
	function initiateAndCancelPayment(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink3"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink3"]);
		kony.automation.playback.waitFor(["frmEuropeTransferToAccountSM","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEuropeTransferToAccountSM","customHeader","flxBack"]);
	}
	
	function verifyCheckBookNavigation(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow9"]);
		kony.automation.playback.waitFor(["frmCMReview","customHeader","flxBack"]);
		kony.automation.widget.touch(["frmCMReview","customHeader","flxBack"], null,null,[27,39]);
	}
	
	function verifyNavigationToSettings(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow10"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow10"]);
		kony.automation.device.deviceBack();
	}
	
	function verifyNavigationViewStatement(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function VerifyAccInfo(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","customHeader","imgSearch"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","customHeader","imgSearch"], null,null,[11,9]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountInfo","information","lblTab1Header"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab1Header"], "text")).not.toBe('');
	}
	
	function VerifyPendingWithdrawal(){
		expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab1Field4Label"], "text")).toContain("Pending Withdrawals");  
	}
	
	function VerifyInterestDetails(){
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab2Field2Label"], "text")).toContain("Interest");
	}
	
	function VerifySwiftCode(){
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab3Field4Label"], "text")).toContain("SWIFT Code");
	}
	function MoveBackFromAccInfo(){
	
	  kony.automation.playback.waitFor(["frmAccountInfo","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccountInfo","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function verifyEditAccountNickname(NickName){
	
	  kony.automation.playback.waitFor(["frmAccountInfo","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmAccountInfo","customHeader","btnRight"]);
	  kony.automation.playback.waitFor(["frmAccountInfo","btnEditNickName"],15000);
	  kony.automation.button.click(["frmAccountInfo","btnEditNickName"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccInfoEdit","txtNickName"],15000);
	  kony.automation.textbox.enterText(["frmAccInfoEdit","txtNickName"],NickName);
	  kony.automation.playback.waitFor(["frmAccInfoEdit","btnSave"],15000);
	  kony.automation.button.click(["frmAccInfoEdit","btnSave"]);
	  kony.automation.playback.wait(5000);
	
	  MoveBackFromAccInfo();
	
	}
	function MoveBackfromAccountDetails(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	//   kony.automation.playback.waitFor(["frmAccountDetails","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.click(["frmAccountDetails","Hamburger","segHamburger[0,0]"]);
	//   kony.automation.playback.wait(5000);
	}
	
	function VerifyMoreoptionsDisplayed(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[57,28]);
	
	  kony.automation.playback.waitFor(["frmAccountDetails","flxCancel"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxCancel"]);
	}
	
	function VerifyOptionsOnLandingScreen(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink1"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink2"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink3"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink4"],15000);
	
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).not.toBe('');
	
	}
	
	function viewAccountStatement(){
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[62,46]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [305,78],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function viewLoanAccountStatement() {
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [192,98],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function viewDepositAccountStatement() {
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [192,98],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function VerifyPendingTranscations(){
	
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	
	  try{
	    let isAvailable=kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,-1]","lblHeaderName"], "text");
	
	    kony.print("is Available "+isAvailable);
	
	    if(isAvailable.includes('Pending')){
	
	      kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,0]"]);
	      kony.automation.playback.wait(5000);
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],15000);
	      kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	      kony.automation.playback.wait(5000);
	
	      MoveBackfromAccountDetails();
	    }else{
	
	      MoveBackfromAccountDetails();
	    }
	  }catch(Exception){
	    MoveBackfromAccountDetails();
	  }
	
	  //   kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  //   kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,0]"]);
	
	  //   kony.automation.playback.wait(5000);
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.playback.wait(5000);
	}
	
	function VerifyPostedTranscations(){
	
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	
	  try{
	    let isAvailable=kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,-1]","lblHeaderName"], "text");
	
	    kony.print("is Available "+isAvailable);
	
	    if(isAvailable.includes('Posted')){
	
	      kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,0]"]);
	      kony.automation.playback.wait(5000);
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],15000);
	      kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	      kony.automation.playback.wait(5000);
	
	      MoveBackfromAccountDetails();
	    }else{
	
	      MoveBackfromAccountDetails();
	    }
	  }catch(Exception){
	    MoveBackfromAccountDetails();
	  }
	
	  //   kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  //   kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,0]"]);
	
	  //   kony.automation.playback.wait(5000);
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.playback.wait(5000);
	
	}
	
	function ScrollDownTranscations(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	  kony.automation.segmentedui.scrollToBottom(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxAddRangeAmount"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","flxAddRangeAmount"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountFrom"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountFrom"],"1");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountTo"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountTo"],"100");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function verofyAdvancedSearch_BlockedFunds(){
		kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	  
		kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,1]"]);
		kony.automation.calendar.selectDate(["frmAdvanceSearch","calStartDate"], [3,1,2021]);
		kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
		
		kony.automation.playback.waitFor(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList"],10000);
		let noRecords = kony.automation.widget.getWidgetProperty(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList[0,0]","lblNoRecords"], "isVisible")
	
		if(!noRecords){
			kony.automation.segmentedui.click(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList[0,0]"]);
		kony.automation.playback.waitFor(["frmMMTransactionDetails","accountsTransactionDetailsNative","lblStatus"],10000);
		expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","accountsTransactionDetailsNative","lblStatus"], "text")).toEqual("BLOCKED");
		kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
		}
		
		kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
		kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	}
	
	function VerifyAdvancedSearch_ByAmount(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxAddRangeAmount"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","flxAddRangeAmount"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountFrom"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountFrom"],"1");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountTo"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountTo"],"9999");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByDate(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTimeRangeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTimeRangeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTimeRange[0,5]"]);
	
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByTransactionType(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTransactionTypeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTransactionTypeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTransactionType[0,3]"]);
	  
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByDebitCredit(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTransactionTypeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTransactionTypeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTransactionType[0,0]"]);
	  
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByKeyword(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","tbxSearch"],15000);
		kony.automation.textbox.enterText(["frmAdvanceSearch","tbxSearch"],"Transfer");
	  kony.automation.playback.wait(3000);
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	 kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifySavingsAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[54,27]);
	  kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow6"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyCheckingAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[54,27]);
	  kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow6"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyCreditCardAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink3"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink3"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyLoanAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyDepositAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function initiateTransfer(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink2"], "text")).toContain("Transfer");	
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);	
	}
	
	function verifyQuickLinkAccessForTransfer(){
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink2"], "text")).toContain("Transfer");	
	}
	
	function createSavingsPotGoal(goalName){
	 /// kony.automation.playback.wait(4000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		
		if(kony.automation.playback.waitFor(["frmMySavingsPot","btnCreateSavingsPot1"],10000)){
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot1"]);
		}else{
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot"]);
		}
		
		kony.automation.playback.waitFor(["frmSavingsType","btnGoal"],15000);
		kony.automation.button.click(["frmSavingsType","btnGoal"]);
		kony.automation.playback.waitFor(["frmGoalsType","segGoalsType"],15000);
		kony.automation.segmentedui.click(["frmGoalsType","segGoalsType[0,0]"]);
		kony.automation.playback.waitFor(["frmGoalName","txtBox"],15000);
		kony.automation.textbox.enterText(["frmGoalName","txtBox"],goalName);
		kony.automation.button.click(["frmGoalName","btnContinue"]);
		kony.automation.playback.waitFor(["frmOptimizeGoal","flxAmountWrapper"],15000);
		kony.automation.flexcontainer.click(["frmOptimizeGoal","flxAmountWrapper"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnTwo"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnZero"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnZero"]);
		kony.automation.widget.touch(["frmOptimizeGoal","lblDone"], [24,16],null,null);
		kony.automation.slider.slide(["frmOptimizeGoal","SliderMonth"], 66);
		kony.automation.button.click(["frmOptimizeGoal","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateSavingsGoalFrequency","btnSave"],15000);
		kony.automation.button.click(["frmCreateSavingsGoalFrequency","btnSave"]);
		kony.automation.playback.waitFor(["frmCreateSavingsGoalFrequencyDate","customCalendar"],15000);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar"], null,null,[356,15]);
		kony.automation.flexcontainer.click(["frmCreateSavingsGoalFrequencyDate","customCalendar","flxNextMonth"]);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar"], null,null,[196,156]);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar","m3CopyLabel0a7f34907bda844"], null,null,[21,16]);
		kony.automation.button.click(["frmCreateSavingsGoalFrequencyDate","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateGoalVerifyDetails","btnContinue"],15000);
		kony.automation.button.click(["frmCreateGoalVerifyDetails","btnContinue"]);
		kony.automation.playback.waitFor(["frmGoalsAcknowledgement","btnFund"],15000);
		kony.automation.button.click(["frmGoalsAcknowledgement","btnFund"]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);
	}
	
	function createSavingsPotBudget(budgetName){
	  //kony.automation.playback.wait(4000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		
		if(kony.automation.playback.waitFor(["frmMySavingsPot","btnCreateSavingsPot1"],10000)){
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot1"]);
		}else{
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot"]);
		}
		
		kony.automation.playback.waitFor(["frmSavingsType","btnBudget"],15000);
		kony.automation.button.click(["frmSavingsType","btnBudget"]);
		kony.automation.playback.waitFor(["frmBudgetName","txtBox"],15000);
		kony.automation.textbox.enterText(["frmBudgetName","txtBox"],budgetName);
		kony.automation.button.click(["frmBudgetName","btnContinue"]);
		kony.automation.playback.waitFor(["frmBudgetfundAmount","keypad","btnTwo"],15000);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnTwo"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateBudgetVerifyDetails","btnContinue"],15000);
		kony.automation.button.click(["frmCreateBudgetVerifyDetails","btnContinue"]);
		kony.automation.playback.waitFor(["frmBudgetAcknowledgement","lblSkip"],15000);
		kony.automation.widget.touch(["frmBudgetAcknowledgement","lblSkip"], null,null,[49,10]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);  
	}
	
	function updateSavingsPot(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		kony.automation.playback.waitFor(["frmMySavingsPot","segMyGoals"],15000);
		kony.automation.segmentedui.click(["frmMySavingsPot","segMyGoals[0,0]"]);
		kony.automation.playback.waitFor(["frmSavingsGoalViewDetails","customHeader","btnRight"],15000);
		kony.automation.button.click(["frmSavingsGoalViewDetails","customHeader","btnRight"]);
		kony.automation.playback.waitFor(["frmEditSavingsGoal","btnSaveConfirm"],15000);
		kony.automation.button.click(["frmEditSavingsGoal","btnSaveConfirm"]);
		kony.automation.playback.waitFor(["frmEditGoalsAcknowledgement","btnFund"],15000);
		kony.automation.button.click(["frmEditGoalsAcknowledgement","btnFund"]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);
	}
	
	
	function clickDisputedTransactions(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[42,25]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow7"]);
	}
	
	function selectDisputedTransaction(){
		kony.automation.playback.waitFor(["frmDisputedTransactionsList","segAccounts"],15000);
		kony.automation.segmentedui.click(["frmDisputedTransactionsList","segAccounts[0,0]"]);
	}
	
	function clickOnSendMessageButton(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","btnSendMessage"],15000);
		kony.automation.button.click(["frmDisputeTransactionDetails","btnSendMessage"]);
	}
	
	
	function verifyTrasactionDetails_Dispute(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","lblReferenceValue"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmDisputeTransactionDetails","lblReferenceValue"], "text")).not.toBeNull();
	}
	
	function goBackFromTransactionDetails_Dispute(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmDisputeTransactionDetails","customHeader","flxBack"]);
	}
	
	function goBackFromDisputeTransactions(){
		kony.automation.playback.waitFor(["frmDisputedTransactionsList","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmDisputedTransactionsList","customHeader","flxBack"]);
	}
	
	function VerifyAccountsDashBoard() {
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard"],30000);
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","lblSelectedAccountType"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","lblSelectedAccountType"], "text")).not.toBe('');
	}
	
	function VerifySwipeOperationOnDashBoard(){
	
	  //kony.automation.scrollToWidget(["frmUnifiedDashboard","lblBarTitle"]);
	  kony.automation.scrollToWidget(["frmUnifiedDashboard","lblNetWorthSummary"]);
	}
	
	function VerifyNotchOperationOnDashBoard(){
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","flxInnerChartSizeToggle"],15000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","flxInnerChartSizeToggle"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","flxInnerChartSizeToggle"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function NavigateToViewAllTranscations() {
	
	  // Scroll to View All form
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","btnViewTransactionsGraph"],15000);
	  kony.automation.scrollToWidget(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
	  kony.automation.button.click(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
	}
	
	function SelectUncategorizedTranscations(){
	
	  // Select Uncategorized Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","flxDropdownImage"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","flxDropdownImage"]);
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactionTypes"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactionTypes[0,1]"]);
	}
	
	function SelectCategorizedTranscations(){
	
	  // Select Categorized Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","flxDropdownImage"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","flxDropdownImage"]);
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactionTypes"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactionTypes[0,0]"]);
	}
	
	function VerifySearchFunctionality_ViewAllTranscation(){
	
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","tbxSearch"],15000);
	  kony.automation.textbox.enterText(["frmPFMCategorisedTransactions","tbxSearch"],"Spent");
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMCategorisedTransactions","segTransactions[0,0]","lblTransaction"], "text")).toContain("Spent");
	}
	
	function MoveBackfromViewAllTranscations(){
	
	  //MoveBack from viewAll Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","customHeader","flxBack"]);
	
	  VerifyAccountsDashBoard();
	}
	
	function MoveBackfromViewAllTranscations_MyMoney(){
	
	  //MoveBack from viewAll Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","customHeader","flxBack"]);
	
	}
	
	function VerifyTranscationDetails(){
	
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactions[0,0]"]);
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","lblLocateUs"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblTransferredToTrans"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblTransferredToTrans"], "text")).not.toBe('');
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblTransferredFromTrans"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblTransferredFromTrans"], "text")).not.toBe('');
	}
	
	function MoveBackFromTranscationDetails(){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","customHeader","flxBack"]);
	  MoveBackfromViewAllTranscations();
	  VerifyAccountsDashBoard();
	}
	
	function MoveBackFromTranscationDetails_MyMoney(){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","customHeader","flxBack"]);
	  MoveBackfromViewAllTranscations_MyMoney();
	}
	
	function EditTranscationDetails(Note){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmPFMTransactionDetails","customHeader","btnRight"]);
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","flxOption2"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","flxOption2"]);
	  kony.automation.playback.waitFor(["frmPFMNote","txtNote"],15000);
	  kony.automation.textarea.enterText(["frmPFMNote","txtNote"],Note);
	  kony.automation.playback.waitFor(["frmPFMNote","btnSave"],15000);
	  kony.automation.button.click(["frmPFMNote","btnSave"]);
	
	}
	
	function VerifyAccountsOrder(){
	
	
	}
	
	function openMenu(menu){
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"]);
	  var menuOptions = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","Hamburger","segHamburger"], "data");
	  kony.print("menuOptions: "+menuOptions);
	  var menuIndex = -1;
	  for(i=0; i<menuOptions.length; i++){
	    if(menuOptions[i].text === menu){
	      menuIndex = i;
	      break;
	    }
	  }
	  if(menuIndex > -1){
	    kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0," + menuIndex+ "]" ]);
	    kony.automation.playback.wait(10000);
	  }else{
	    expect(menuIndex).toBeGreaterThan(-1);
	  }
	}
	
	
	
	
	function navigateToUTF(){
	  openMenu("Unified Transfers flow");
	}
	
	function selectMakeTransferWithinSameBank() {
		kony.automation.playback.waitFor(["frmSelectTransferType","transferTypeSelection1","btnAction1"],15000);
		kony.automation.playback.wait(3000);
		kony.automation.button.click(["frmSelectTransferType","transferTypeSelection1","btnAction1"]);
	}
	
	function selectMakeTransferDomesticAccount(){
		kony.automation.playback.waitFor(["frmSelectTransferType","transferTypeSelection2","btnAction1"],15000);
		kony.automation.playback.wait(3000);
		kony.automation.button.click(["frmSelectTransferType","transferTypeSelection2","btnAction1"]);  
	}
	
	function selectMakeTransferInternational(){
		kony.automation.playback.waitFor(["frmSelectTransferType","transferTypeSelection3","btnAction1"],15000);
		kony.automation.playback.wait(3000);
		kony.automation.button.click(["frmSelectTransferType","transferTypeSelection3","btnAction1"]);  
	}
	
	function SelectTransferFromAccount(ToAccReciptent){
	  kony.automation.playback.wait(4000);
	  let currFormName = kony.automation.getCurrentForm();
	  let index = {"i" : 0 , "j" : 0};
	
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","tbxFromSearch"],15000);
		kony.automation.textbox.enterText([currFormName,"MakeATransfer","tbxFromSearch"],ToAccReciptent);
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","segFromAccounts"],15000);
	
		let  segData = kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segFromAccounts"], "data");
	  index = getIndexOfAccount_UTF(segData , ToAccReciptent);
	  kony.automation.segmentedui.scrollToRow([currFormName,"MakeATransfer","segFromAccounts["+index.i +"," + index.j + "]"]);
	
	  expect(kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segFromAccounts["+index.i +"," + index.j + "]","lblField1"], "text")).not.toBe('');
	  kony.automation.playback.waitFor([currFormName,"MakeATransfer","segFromAccounts"],15000);
	  kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFromAccounts["+index.i +"," + index.j + "]"]);
	
	}
	
	function SelectTransferToAccount(ToAccReciptent){
	  kony.automation.playback.wait(4000);
	  let currFormName = kony.automation.getCurrentForm();
	  let index = {"i" : 0 , "j" : 0};
	
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","tbxToSearch"],15000);
		kony.automation.textbox.enterText([currFormName,"MakeATransfer","tbxToSearch"],ToAccReciptent);
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","segToAccounts"],15000);
	
		let  segData = kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segToAccounts"], "data");
	  index = getIndexOfAccount_UTF(segData , ToAccReciptent);
	  kony.automation.segmentedui.scrollToRow([currFormName,"MakeATransfer","segToAccounts["+index.i +"," + index.j + "]"]);
	
	  expect(kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segToAccounts["+index.i +"," + index.j + "]","lblField1"], "text")).not.toBe('');
	  kony.automation.playback.waitFor([currFormName,"MakeATransfer","segToAccounts"],15000);
	  kony.automation.segmentedui.click([currFormName,"MakeATransfer","segToAccounts["+index.i +"," + index.j + "]"]);
	
	}
	
	function enterAmount(Amount) {
	  kony.automation.playback.wait(3000);
	  let currFormName = kony.automation.getCurrentForm();
	  kony.automation.playback.waitFor([currFormName,"MakeATransfer","btnThree"],15000);
	  for(i=0; i<Amount.length; i++){
	    kony.automation.button.click([currFormName,"MakeATransfer", getBtnID(Amount.charAt(i))]);
	  }
	  kony.automation.playback.waitFor([currFormName,"MakeATransfer", "btnContinue"],3000);
	  kony.automation.button.click([currFormName,"MakeATransfer","btnContinue"]);
	  kony.automation.playback.wait(5000);
	}
	
	function selectFrequency(ValTimePeriod) {
	   kony.automation.playback.wait(5000);
	  let currFormName = kony.automation.getCurrentForm();
	
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","flxField4"],15000);
		kony.automation.flexcontainer.click([currFormName,"MakeATransfer","flxField4"]);
		kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,1]"]);
	
	  kony.automation.playback.waitFor([currFormName,"MakeATransfer","segFrequencyOptions"],15000);
	  switch(ValTimePeriod){
	    case "Daily":
	      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,1]"]);
	      break;
	    case "Weekly":
	      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,2]"]);
	      break;
	    case "HalfY":
	      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,6]"]);
	      break;
	    case "Yearly":
	      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,7]"]);
	      break;
	    case "QTR":
	      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,5]"]);
	      break;
	    case "Monthly":
	      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,4]"]);
	      break;
	  }
	
	  kony.automation.playback.wait(5000);
	}
	
	function confirmTransfer(){
		kony.automation.playback.wait(5000);
		let currFormName = kony.automation.getCurrentForm();
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","imgAddIcon"],15000);
		kony.automation.widget.touch([currFormName,"MakeATransfer","imgAddIcon"], [7,11],null,null);
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","flxSelectOptionsCancel"],15000);
		kony.automation.widget.touch([currFormName,"MakeATransfer","flxSelectOptionsCancel"], [117,38],null,null);
		kony.automation.playback.waitFor([currFormName,"MakeATransfer","btnTransfer"],15000);
		kony.automation.button.click([currFormName,"MakeATransfer","btnTransfer"]);
	}
	
	function verifyTransferSuccessMessage() {
		
		kony.automation.playback.waitFor(["flxSameBankAcknowledgement","Acknowledgement","lblSuccess"],10000);
		let currFormName = kony.automation.getCurrentForm();	
	  expect(kony.automation.widget.getWidgetProperty([currFormName,"Acknowledgement","lblSuccess"], "text")).not.toBe('');
		kony.automation.playback.waitFor([currFormName,"Acknowledgement","flxFail"],5000);
		let flxError = kony.automation.widget.getWidgetProperty([currFormName,"Acknowledgement","flxFail"], "isVisible");
		if(flxError){
	      expect("Transaction").toBe("successful");
	      kony.automation.button.click([currFormName, "Acknowledgement","btnFailureAction1"]);
	      kony.automation.playback.wait(1000);	
	      // kony.automation.playback.waitFor(["frmEuropeTransferFromAccount","customHeader","btnRight"],15000);
	      // kony.automation.button.click(["frmEuropeTransferFromAccount","customHeader","btnRight"]);
		}
		else{
			kony.automation.playback.waitFor([currFormName,"Acknowledgement","btnSuccessAction1"],15000);
			kony.automation.button.click([currFormName,"Acknowledgement","btnSuccessAction1"]);
			kony.automation.playback.waitFor(["frmSelectTransferType","imgBack"],15000);
			kony.automation.widget.touch(["frmSelectTransferType","imgBack"], [14,13],null,null);
			kony.automation.playback.wait(5000);
	    }
		VerifyAccountsDashBoard();
	}
	
	function goToDashboardFromUTF(){
		kony.automation.playback.waitFor(["frmSelectTransferType","imgBack"],15000);
		kony.automation.widget.touch(["frmSelectTransferType","imgBack"], [19,16],null,null);
		let flag = kony.automation.playback.waitFor(["frmSelectTransferType","imgBack"],3000);
		if(flag){
			kony.automation.widget.touch(["frmSelectTransferType","imgBack"], [10,18],null,null);      
	    }
		VerifyAccountsDashBoard();
	}
	
	function cancelEnterAmount(){
	  kony.automation.playback.wait(3000);
	  let currFormName = kony.automation.getCurrentForm();	
	  kony.automation.button.click([currFormName,"MakeATransfer","btnAmountCancel"]);
	}
	
	function getIndexFromSegment(segData , val){
		for(let i=0; i < segData.length; i++){
			if(segData[i].property === val){
				return i;
			}
		}
		return 0; //default
	}
	
	function getBtnID(num){
	  switch(num){
	    case '0' :
	      return "btnZero";
	    case '1' :
	      return "btnOne";
	    case '2' :
	      return "btnTwo";
	    case '3' :
	      return "btnThree";
	    case '4' :
	      return "btnFour";
	    case '5' :
	      return "btnFive";
	    case '6' :
	      return "btnSix";
	    case '7' :
	      return "btnSeven";
	    case '8' :
	      return "btnEight";
	    case '9' :
	      return "btnNine";
	  }
	}
	
	function getIndexOfAccount_UTF(segData , accountName){
	  let i=0;
	  let j=0;
	  kony.print("segData : "+JSON.stringify(segData));
	  for(i=0; i<segData.length; i++){
	      if(segData[i].length > 1){
	          for(j=0; j<segData[i][1].length; j++){
	
	              if(segData[i][1][j].lblField1.text.includes(accountName)){
	                  return { i, j};
	              }
	          }
	      }
	  }
	  return {i,j };
	}
	
	it("SearchToAndFromAccount", async function() {
	  let fromAccount = "8648";
	  let toAccount = "Savings";
	  navigateToUTF();
	  selectMakeTransferWithinSameBank();
	  SelectTransferFromAccount(fromAccount);
	  SelectTransferToAccount(toAccount);
	  cancelEnterAmount();
	  goToDashboardFromUTF();
	});
	
	it("SearchToAndFromAccount", async function() {
	  let fromAccount = "8648";
	  let toAccount = "International";
	  navigateToUTF();
	  selectMakeTransferInternational();
	  SelectTransferFromAccount(fromAccount);
	  SelectTransferToAccount(toAccount);
	  cancelEnterAmount();
	  goToDashboardFromUTF();
	});
	
	it("SearchToAndFromAccount", async function() {
	  let fromAccount = "8648";
	  let toAccount = "Domestic";
	  navigateToUTF();
	  selectMakeTransferDomesticAccount();
	  SelectTransferFromAccount(fromAccount);
	  SelectTransferToAccount(toAccount);
	  cancelEnterAmount();
	  goToDashboardFromUTF();
	});
	
	it("OneTimeTransaferInternational", async function() {
	  let fromAccount = "8648";
	  let toAccount = "International";
		navigateToUTF();
	  selectMakeTransferInternational();
	  SelectTransferFromAccount(fromAccount);
	  SelectTransferToAccount(toAccount);
	  enterAmount("200");
	//   selectFrequency("Daily");
	  confirmTransfer();
	  verifyTransferSuccessMessage();  
	},12000);
	
	it("OneTimeTransaferSameBank", async function() {
	  let fromAccount = "8648";
	  let toAccount = "Savings";
		navigateToUTF();
	  selectMakeTransferWithinSameBank();
	  SelectTransferFromAccount(fromAccount);
	  SelectTransferToAccount(toAccount);
	  enterAmount("200");
	//   selectFrequency("Daily");
	  confirmTransfer();
	  verifyTransferSuccessMessage();  
	},12000);
	
	it("OneTimeTransaferDomestic", async function() {
	  let fromAccount = "8648";
	  let toAccount = "Domestic";
		navigateToUTF();
	  selectMakeTransferDomesticAccount();
	  SelectTransferFromAccount(fromAccount);
	  SelectTransferToAccount(toAccount);
	  enterAmount("200");
	//   selectFrequency("Daily");
	  confirmTransfer();
	  verifyTransferSuccessMessage();  
	},12000);
});