describe("Settings", function() {
	beforeEach(async function() {
	// 	jasmine.DEFAULT_TIMEOUT_INTERVAL = 90000;
	    var flgLoginForm = await kony.automation.playback.waitFor(["frmLogin", "login", "btnLogIn"], 2000);
	    var flgLogoutForm = await kony.automation.playback.waitFor(["frmLogout", "btnLogIn"], 2000);
	    kony.print("flgLoginForm: " + flgLoginForm + " ,flgLogoutForm: " + flgLogoutForm);
	
	    if (flgLogoutForm) {
	        kony.automation.button.click(["frmLogout", "btnLogIn"]);
	        kony.automation.playback.waitFor(["frmLogin", "login", "tbxUsername"], 10000);
	        await kony.automation.playback.wait(3000);
	//         expect(kony.automation.widget.getWidgetProperty(["frmLogin", "login", "tbxPassword"], "text")).toEqual("");
	        await login(LoginDetails.username);
	    } else if (flgLoginForm === true || flgLoginForm === 1) {
	        await login(LoginDetails.username);
	    }
	},12000);
	
	
	async function login(username) {
	    await kony.automation.playback.waitFor(["frmLogin", "login", "tbxUsername"]);
	    kony.automation.textbox.enterText(["frmLogin", "login", "tbxUsername"], username);
	    kony.automation.textbox.enterText(["frmLogin", "login", "tbxPassword"], LoginDetails.password);
	    await kony.automation.playback.waitFor(["frmLogin", "login", "btnLogIn"]);
	    kony.automation.button.click(["frmLogin", "login", "btnLogIn"]);
	    //Verifying Terms and Condition page -
	    var frmTnC = await kony.automation.playback.waitFor(["frmTermsAndCondition", "flxCheckBox"], 20000);
	    if (frmTnC) {
	        kony.automation.flexcontainer.click(["frmTermsAndCondition", "flxCheckBox"]);
	        await kony.automation.playback.waitFor(["frmTermsAndCondition", "btnContinue"]);
	        kony.automation.button.click(["frmTermsAndCondition", "btnContinue"]);
	    }
	
	    await kony.automation.playback.waitFor(["frmUnifiedDashboard", "lblBankName"], 15000);
	}
	
	function VerifyAccountsDashBoard() {
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard"],30000);
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","lblSelectedAccountType"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","lblSelectedAccountType"], "text")).not.toBe('');
	}
	
	function VerifySwipeOperationOnDashBoard(){
	
	  //kony.automation.scrollToWidget(["frmUnifiedDashboard","lblBarTitle"]);
	  kony.automation.scrollToWidget(["frmUnifiedDashboard","lblNetWorthSummary"]);
	}
	
	function VerifyNotchOperationOnDashBoard(){
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","flxInnerChartSizeToggle"],15000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","flxInnerChartSizeToggle"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","flxInnerChartSizeToggle"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function NavigateToViewAllTranscations() {
	
	  // Scroll to View All form
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","btnViewTransactionsGraph"],15000);
	  kony.automation.scrollToWidget(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
	  kony.automation.button.click(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
	}
	
	function SelectUncategorizedTranscations(){
	
	  // Select Uncategorized Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","flxDropdownImage"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","flxDropdownImage"]);
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactionTypes"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactionTypes[0,1]"]);
	}
	
	function SelectCategorizedTranscations(){
	
	  // Select Categorized Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","flxDropdownImage"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","flxDropdownImage"]);
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactionTypes"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactionTypes[0,0]"]);
	}
	
	function VerifySearchFunctionality_ViewAllTranscation(){
	
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","tbxSearch"],15000);
	  kony.automation.textbox.enterText(["frmPFMCategorisedTransactions","tbxSearch"],"Spent");
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMCategorisedTransactions","segTransactions[0,0]","lblTransaction"], "text")).toContain("Spent");
	}
	
	function MoveBackfromViewAllTranscations(){
	
	  //MoveBack from viewAll Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","customHeader","flxBack"]);
	
	  VerifyAccountsDashBoard();
	}
	
	function MoveBackfromViewAllTranscations_MyMoney(){
	
	  //MoveBack from viewAll Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","customHeader","flxBack"]);
	
	}
	
	function VerifyTranscationDetails(){
	
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactions[0,0]"]);
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","lblLocateUs"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblTransferredToTrans"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblTransferredToTrans"], "text")).not.toBe('');
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblTransferredFromTrans"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblTransferredFromTrans"], "text")).not.toBe('');
	}
	
	function MoveBackFromTranscationDetails(){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","customHeader","flxBack"]);
	  MoveBackfromViewAllTranscations();
	  VerifyAccountsDashBoard();
	}
	
	function MoveBackFromTranscationDetails_MyMoney(){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","customHeader","flxBack"]);
	  MoveBackfromViewAllTranscations_MyMoney();
	}
	
	function EditTranscationDetails(Note){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmPFMTransactionDetails","customHeader","btnRight"]);
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","flxOption2"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","flxOption2"]);
	  kony.automation.playback.waitFor(["frmPFMNote","txtNote"],15000);
	  kony.automation.textarea.enterText(["frmPFMNote","txtNote"],Note);
	  kony.automation.playback.waitFor(["frmPFMNote","btnSave"],15000);
	  kony.automation.button.click(["frmPFMNote","btnSave"]);
	
	}
	
	function VerifyAccountsOrder(){
	
	
	}
	
	function openMenu(menu){
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"]);
	  var menuOptions = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","Hamburger","segHamburger"], "data");
	  kony.print("menuOptions: "+menuOptions);
	  var menuIndex = -1;
	  for(i=0; i<menuOptions.length; i++){
	    if(menuOptions[i].text === menu){
	      menuIndex = i;
	      break;
	    }
	  }
	  if(menuIndex > -1){
	    kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0," + menuIndex+ "]" ]);
	    kony.automation.playback.wait(10000);
	  }else{
	    expect(menuIndex).toBeGreaterThan(-1);
	  }
	}
	
	
	
	
	function SelectAccountOndashBoard(account){
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","segAccounts"],15000);
	  let indices = getIndex(account);
	  kony.automation.widget.touch(["frmUnifiedDashboard","segAccounts"], null,null,[80,97]);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","segAccounts[" + indices.i + "," + indices.j +"]"]);
	  kony.automation.playback.wait(10000);
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","lblAccountName"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountSummaryNative","lblAccountName"], "text")).not.toBe('');
	
	}
	
	// function getIndex(account){
	//   let segData = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","segAccounts"], "data");
	//   let i=0;
	//   let j=0;
	
	//   for(var a = 0; a<segData.length; a++){
	//     for(var b=0; b<segData[a][1].length; b++){
	//       if(segData[a][1][b].accountName.includes(account)){
	//         i=a;
	//         j=b;
	//         break;
	//       }
	//     }
	//   }
	
	//   return {
	//     i,
	//     j
	//   };
	// }
	
	 function getIndex(account){
	  let segData = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","segAccounts"], "data");
	  let i=0;
	  let j=0;
	
	  for(var a = 0; a<segData.length; a++){
	    for(var b=0; b<segData[a][1].length; b++){
	      if(segData[a][1][b].accountName === undefined){
	        kony.automation.flexcontainer.click(["frmUnifiedDashboard","segAccounts[" + a + "," + b +"]","flxViewContainer"]);
	        kony.automation.playback.wait(2000);
	        return getIndex(account);                                   
	
	      }else{
	        if(segData[a][1][b].accountName.includes(account)){
	          i=a;
	          j=b;
	          break;
	        }                               
	      }
	    }
	  }
	  return {
	    i,
	    j
	  };
	}
	
	function ClickonFirstSavingAccount(){
	
	  SelectAccountOndashBoard(Transfers.savingsAccount.name);
	
	}
	
	function ClickonFirstCheckingAccount(){
	
	  SelectAccountOndashBoard(Transfers.checkingAccount.name);
	}
	
	function ClickonFirstLoanAccount(){
	
	  SelectAccountOndashBoard(Transfers.loanAccount.name);
	}
	
	function ClickonFirstCreditCardAccount(){
	
	  SelectAccountOndashBoard("Credit Card");
	}
	
	function ClickonFirstDepositAccount(){
	
	  SelectAccountOndashBoard(Transfers.depositAccount.name);
	}
	
	function initiateAndCancelTransfer(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
		kony.automation.playback.waitFor(["frmEuropeTransferToAccount","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEuropeTransferToAccount","customHeader","flxBack"]);
	}
	
	function initiateAndCancelPayment(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink3"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink3"]);
		kony.automation.playback.waitFor(["frmEuropeTransferToAccountSM","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEuropeTransferToAccountSM","customHeader","flxBack"]);
	}
	
	function verifyCheckBookNavigation(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow9"]);
		kony.automation.playback.waitFor(["frmCMReview","customHeader","flxBack"]);
		kony.automation.widget.touch(["frmCMReview","customHeader","flxBack"], null,null,[27,39]);
	}
	
	function verifyNavigationToSettings(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow10"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow10"]);
		kony.automation.device.deviceBack();
	}
	
	function verifyNavigationViewStatement(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function VerifyAccInfo(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","customHeader","imgSearch"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","customHeader","imgSearch"], null,null,[11,9]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountInfo","information","lblTab1Header"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab1Header"], "text")).not.toBe('');
	}
	
	function VerifyPendingWithdrawal(){
		expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab1Field4Label"], "text")).toContain("Pending Withdrawals");  
	}
	
	function VerifyInterestDetails(){
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab2Field2Label"], "text")).toContain("Interest");
	}
	
	function VerifySwiftCode(){
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab3Field4Label"], "text")).toContain("SWIFT Code");
	}
	function MoveBackFromAccInfo(){
	
	  kony.automation.playback.waitFor(["frmAccountInfo","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccountInfo","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function verifyEditAccountNickname(NickName){
	
	  kony.automation.playback.waitFor(["frmAccountInfo","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmAccountInfo","customHeader","btnRight"]);
	  kony.automation.playback.waitFor(["frmAccountInfo","btnEditNickName"],15000);
	  kony.automation.button.click(["frmAccountInfo","btnEditNickName"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccInfoEdit","txtNickName"],15000);
	  kony.automation.textbox.enterText(["frmAccInfoEdit","txtNickName"],NickName);
	  kony.automation.playback.waitFor(["frmAccInfoEdit","btnSave"],15000);
	  kony.automation.button.click(["frmAccInfoEdit","btnSave"]);
	  kony.automation.playback.wait(5000);
	
	  MoveBackFromAccInfo();
	
	}
	function MoveBackfromAccountDetails(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	//   kony.automation.playback.waitFor(["frmAccountDetails","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.click(["frmAccountDetails","Hamburger","segHamburger[0,0]"]);
	//   kony.automation.playback.wait(5000);
	}
	
	function VerifyMoreoptionsDisplayed(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[57,28]);
	
	  kony.automation.playback.waitFor(["frmAccountDetails","flxCancel"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxCancel"]);
	}
	
	function VerifyOptionsOnLandingScreen(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink1"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink2"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink3"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink4"],15000);
	
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).not.toBe('');
	
	}
	
	function viewAccountStatement(){
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[62,46]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [305,78],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function viewLoanAccountStatement() {
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [192,98],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function viewDepositAccountStatement() {
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [192,98],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function VerifyPendingTranscations(){
	
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	
	  try{
	    let isAvailable=kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,-1]","lblHeaderName"], "text");
	
	    kony.print("is Available "+isAvailable);
	
	    if(isAvailable.includes('Pending')){
	
	      kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,0]"]);
	      kony.automation.playback.wait(5000);
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],15000);
	      kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	      kony.automation.playback.wait(5000);
	
	      MoveBackfromAccountDetails();
	    }else{
	
	      MoveBackfromAccountDetails();
	    }
	  }catch(Exception){
	    MoveBackfromAccountDetails();
	  }
	
	  //   kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  //   kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,0]"]);
	
	  //   kony.automation.playback.wait(5000);
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.playback.wait(5000);
	}
	
	function VerifyPostedTranscations(){
	
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	
	  try{
	    let isAvailable=kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,-1]","lblHeaderName"], "text");
	
	    kony.print("is Available "+isAvailable);
	
	    if(isAvailable.includes('Posted')){
	
	      kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,0]"]);
	      kony.automation.playback.wait(5000);
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],15000);
	      kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	      kony.automation.playback.wait(5000);
	
	      MoveBackfromAccountDetails();
	    }else{
	
	      MoveBackfromAccountDetails();
	    }
	  }catch(Exception){
	    MoveBackfromAccountDetails();
	  }
	
	  //   kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  //   kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,0]"]);
	
	  //   kony.automation.playback.wait(5000);
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.playback.wait(5000);
	
	}
	
	function ScrollDownTranscations(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	  kony.automation.segmentedui.scrollToBottom(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxAddRangeAmount"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","flxAddRangeAmount"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountFrom"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountFrom"],"1");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountTo"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountTo"],"100");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function verofyAdvancedSearch_BlockedFunds(){
		kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	  
		kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,1]"]);
		kony.automation.calendar.selectDate(["frmAdvanceSearch","calStartDate"], [3,1,2021]);
		kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
		
		kony.automation.playback.waitFor(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList"],10000);
		let noRecords = kony.automation.widget.getWidgetProperty(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList[0,0]","lblNoRecords"], "isVisible")
	
		if(!noRecords){
			kony.automation.segmentedui.click(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList[0,0]"]);
		kony.automation.playback.waitFor(["frmMMTransactionDetails","accountsTransactionDetailsNative","lblStatus"],10000);
		expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","accountsTransactionDetailsNative","lblStatus"], "text")).toEqual("BLOCKED");
		kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
		}
		
		kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
		kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	}
	
	function VerifyAdvancedSearch_ByAmount(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxAddRangeAmount"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","flxAddRangeAmount"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountFrom"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountFrom"],"1");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountTo"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountTo"],"9999");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByDate(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTimeRangeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTimeRangeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTimeRange[0,5]"]);
	
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByTransactionType(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTransactionTypeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTransactionTypeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTransactionType[0,3]"]);
	  
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByDebitCredit(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTransactionTypeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTransactionTypeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTransactionType[0,0]"]);
	  
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByKeyword(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","tbxSearch"],15000);
		kony.automation.textbox.enterText(["frmAdvanceSearch","tbxSearch"],"Transfer");
	  kony.automation.playback.wait(3000);
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	 kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifySavingsAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[54,27]);
	  kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow6"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyCheckingAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[54,27]);
	  kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow6"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyCreditCardAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink3"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink3"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyLoanAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyDepositAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function initiateTransfer(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink2"], "text")).toContain("Transfer");	
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);	
	}
	
	function verifyQuickLinkAccessForTransfer(){
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink2"], "text")).toContain("Transfer");	
	}
	
	function createSavingsPotGoal(goalName){
	 /// kony.automation.playback.wait(4000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		
		if(kony.automation.playback.waitFor(["frmMySavingsPot","btnCreateSavingsPot1"],10000)){
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot1"]);
		}else{
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot"]);
		}
		
		kony.automation.playback.waitFor(["frmSavingsType","btnGoal"],15000);
		kony.automation.button.click(["frmSavingsType","btnGoal"]);
		kony.automation.playback.waitFor(["frmGoalsType","segGoalsType"],15000);
		kony.automation.segmentedui.click(["frmGoalsType","segGoalsType[0,0]"]);
		kony.automation.playback.waitFor(["frmGoalName","txtBox"],15000);
		kony.automation.textbox.enterText(["frmGoalName","txtBox"],goalName);
		kony.automation.button.click(["frmGoalName","btnContinue"]);
		kony.automation.playback.waitFor(["frmOptimizeGoal","flxAmountWrapper"],15000);
		kony.automation.flexcontainer.click(["frmOptimizeGoal","flxAmountWrapper"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnTwo"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnZero"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnZero"]);
		kony.automation.widget.touch(["frmOptimizeGoal","lblDone"], [24,16],null,null);
		kony.automation.slider.slide(["frmOptimizeGoal","SliderMonth"], 66);
		kony.automation.button.click(["frmOptimizeGoal","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateSavingsGoalFrequency","btnSave"],15000);
		kony.automation.button.click(["frmCreateSavingsGoalFrequency","btnSave"]);
		kony.automation.playback.waitFor(["frmCreateSavingsGoalFrequencyDate","customCalendar"],15000);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar"], null,null,[356,15]);
		kony.automation.flexcontainer.click(["frmCreateSavingsGoalFrequencyDate","customCalendar","flxNextMonth"]);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar"], null,null,[196,156]);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar","m3CopyLabel0a7f34907bda844"], null,null,[21,16]);
		kony.automation.button.click(["frmCreateSavingsGoalFrequencyDate","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateGoalVerifyDetails","btnContinue"],15000);
		kony.automation.button.click(["frmCreateGoalVerifyDetails","btnContinue"]);
		kony.automation.playback.waitFor(["frmGoalsAcknowledgement","btnFund"],15000);
		kony.automation.button.click(["frmGoalsAcknowledgement","btnFund"]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);
	}
	
	function createSavingsPotBudget(budgetName){
	  //kony.automation.playback.wait(4000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		
		if(kony.automation.playback.waitFor(["frmMySavingsPot","btnCreateSavingsPot1"],10000)){
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot1"]);
		}else{
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot"]);
		}
		
		kony.automation.playback.waitFor(["frmSavingsType","btnBudget"],15000);
		kony.automation.button.click(["frmSavingsType","btnBudget"]);
		kony.automation.playback.waitFor(["frmBudgetName","txtBox"],15000);
		kony.automation.textbox.enterText(["frmBudgetName","txtBox"],budgetName);
		kony.automation.button.click(["frmBudgetName","btnContinue"]);
		kony.automation.playback.waitFor(["frmBudgetfundAmount","keypad","btnTwo"],15000);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnTwo"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateBudgetVerifyDetails","btnContinue"],15000);
		kony.automation.button.click(["frmCreateBudgetVerifyDetails","btnContinue"]);
		kony.automation.playback.waitFor(["frmBudgetAcknowledgement","lblSkip"],15000);
		kony.automation.widget.touch(["frmBudgetAcknowledgement","lblSkip"], null,null,[49,10]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);  
	}
	
	function updateSavingsPot(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		kony.automation.playback.waitFor(["frmMySavingsPot","segMyGoals"],15000);
		kony.automation.segmentedui.click(["frmMySavingsPot","segMyGoals[0,0]"]);
		kony.automation.playback.waitFor(["frmSavingsGoalViewDetails","customHeader","btnRight"],15000);
		kony.automation.button.click(["frmSavingsGoalViewDetails","customHeader","btnRight"]);
		kony.automation.playback.waitFor(["frmEditSavingsGoal","btnSaveConfirm"],15000);
		kony.automation.button.click(["frmEditSavingsGoal","btnSaveConfirm"]);
		kony.automation.playback.waitFor(["frmEditGoalsAcknowledgement","btnFund"],15000);
		kony.automation.button.click(["frmEditGoalsAcknowledgement","btnFund"]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);
	}
	
	
	function clickDisputedTransactions(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[42,25]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow7"]);
	}
	
	function selectDisputedTransaction(){
		kony.automation.playback.waitFor(["frmDisputedTransactionsList","segAccounts"],15000);
		kony.automation.segmentedui.click(["frmDisputedTransactionsList","segAccounts[0,0]"]);
	}
	
	function clickOnSendMessageButton(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","btnSendMessage"],15000);
		kony.automation.button.click(["frmDisputeTransactionDetails","btnSendMessage"]);
	}
	
	
	function verifyTrasactionDetails_Dispute(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","lblReferenceValue"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmDisputeTransactionDetails","lblReferenceValue"], "text")).not.toBeNull();
	}
	
	function goBackFromTransactionDetails_Dispute(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmDisputeTransactionDetails","customHeader","flxBack"]);
	}
	
	function goBackFromDisputeTransactions(){
		kony.automation.playback.waitFor(["frmDisputedTransactionsList","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmDisputedTransactionsList","customHeader","flxBack"]);
	}
	
	function logout() {
		kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
		kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxLogout"],15000);
		kony.automation.widget.touch(["frmUnifiedDashboard","Hamburger","flxLogout"], null,null,[37,31]);
		kony.automation.playback.waitFor(["frmLogout","btnLogIn"],15000);
		kony.automation.button.click(["frmLogout","btnLogIn"]);
		kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],15000);
	}
	
	function NavigateToSettings(){
	  openMenu("Settings");
	}
	/*
	function enableAccountsPreview(){
	
	  kony.automation.playback.waitFor(["frmSettings","segSettingsLogin"],15000);
	  kony.automation.segmentedui.click(["frmSettings","segSettingsLogin[0,0]"]);
	  kony.automation.playback.waitFor(["frmPreferencesAccountPreview","switchPreview"],15000);
	  kony.automation.switch.toggle(["frmPreferencesAccountPreview","switchPreview"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmPreferencesAccountPreview","switchPreview"],15000);
	  kony.automation.switch.toggle(["frmPreferencesAccountPreview","switchPreview"]);
	
	  kony.automation.playback.waitFor(["frmPreferencesAccountPreview","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPreferencesAccountPreview","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	*/
	
	function enableAccountPreview() {
		kony.automation.playback.waitFor(["frmSettings","segSettingsLogin"],15000);
		kony.automation.segmentedui.click(["frmSettings","segSettingsLogin[0,0]"]);
		kony.automation.playback.waitFor(["frmPreferencesAccountPreview","switchPreview"],15000);
		let selectedIndex = kony.automation.widget.getWidgetProperty(["frmPreferencesAccountPreview","switchPreview"], "selectedIndex");
		if(selectedIndex === 1){
			kony.automation.switch.toggle(["frmPreferencesAccountPreview","switchPreview"]);
		}
		kony.automation.flexcontainer.click(["frmPreferencesAccountPreview","customHeader","flxBack"]);
	}
	
	function disableAccountPreview() {
		kony.automation.playback.waitFor(["frmSettings","segSettingsLogin"],15000);
		kony.automation.segmentedui.click(["frmSettings","segSettingsLogin[0,0]"]);
		kony.automation.playback.waitFor(["frmPreferencesAccountPreview","switchPreview"],15000);
		let selectedIndex = kony.automation.widget.getWidgetProperty(["frmPreferencesAccountPreview","switchPreview"], "selectedIndex");
		if(selectedIndex === 0){
			kony.automation.switch.toggle(["frmPreferencesAccountPreview","switchPreview"]);
		}
		kony.automation.flexcontainer.click(["frmPreferencesAccountPreview","customHeader","flxBack"]);
	}
	
	function verifyPreview() {
		kony.automation.playback.waitFor(["frmLogin","flxDashboard"],15000);
		kony.automation.flexcontainer.click(["frmLogin","flxDashboard"]);
		kony.automation.playback.waitFor(["frmLogin","segAccountPreview"],15000);
		kony.automation.flexcontainer.click(["frmLogin","flxDashboard"]);
	}
	
	function MoveBackFromSettings_DashBoard(){
	
	  kony.automation.playback.waitFor(["frmSettings","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmSettings","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmSettings","Hamburger","segHamburger"],15000);
	  kony.automation.segmentedui.click(["frmSettings","Hamburger","segHamburger[0,0]"]);
	
	  VerifyAccountsDashBoard();
	}
	
	function CancelEditAccountPreference(){
	  kony.automation.playback.waitFor(["frmSettings","segSettingsDefaultAccount"]);
		kony.automation.segmentedui.click(["frmSettings","segSettingsDefaultAccount[0,0]"]);
		kony.automation.playback.waitFor(["frmEStmtAccountPreferences","segSelectAccounts"]);
		kony.automation.segmentedui.click(["frmEStmtAccountPreferences","segSelectAccounts[0,0]"]);
		kony.automation.playback.waitFor(["frmEStmtAccountDetails","customHeader","btnRight"]);
		kony.automation.button.click(["frmEStmtAccountDetails","customHeader","btnRight"]);
		kony.automation.playback.waitFor(["frmEStmtAccountDetails","flxCancel"]);
		kony.automation.flexcontainer.click(["frmEStmtAccountDetails","flxCancel"]);
		kony.automation.flexcontainer.click(["frmEStmtAccountDetails","customHeader","flxBack"]);
		kony.automation.playback.waitFor(["frmEStmtAccountPreferences","customHeader","flxBack"]);
		kony.automation.flexcontainer.click(["frmEStmtAccountPreferences","customHeader","flxBack"]);
	}
	
	function VerifyConsentTypeNotClickable(){ 
	  
	// 	kony.automation.playback.waitFor(["frmSettings","segConsentManagement"],15000);
	// 	kony.automation.segmentedui.click(["frmSettings","segConsentManagement[0,0]"]);
		OpenConsentManagement();
		kony.automation.playback.waitFor(["frmConsentManagement","segConsentTypes"],15000);
		kony.automation.segmentedui.click(["frmConsentManagement","segConsentTypes[0,0]"]);
		
		let frmName = kony.automation.getCurrentForm();
		if(frmName !== "frmConsentManagement"){
		expect(true).toBe(false);
		}
	}
	function MoveBackFromConsentToSetting(){
		kony.automation.playback.waitFor(["frmConsentManagement","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmConsentManagement","customHeader","flxBack"]);
		kony.automation.playback.waitFor(["frmSettings","segConsentManagement"],15000);
	}
	
	function EditAndSaveConsent(){
	// 	kony.automation.playback.waitFor(["frmSettings","segConsentManagement"],15000);
	// 	kony.automation.segmentedui.click(["frmSettings","segConsentManagement[0,0]"]);
	  OpenConsentManagement();
		kony.automation.playback.waitFor(["frmConsentManagement","customHeader","btnRight"],15000);
		kony.automation.button.click(["frmConsentManagement","customHeader","btnRight"]);
		kony.automation.playback.wait(1000);
		// :User Injected Code Snippet [// - [9 lines]]
		let selectedIndex = kony.automation.widget.getWidgetProperty(["frmConsentManagement","segEditConsent[0,0]","switchSelect"], "selectedIndex");
		
		if(selectedIndex === 1){
		kony.automation.switch.toggle(["frmConsentManagement","segEditConsent[0,0]","switchSelect"]);
		kony.automation.scrollToWidget(["frmConsentManagement","btnSave"]);
			kony.automation.button.click(["frmConsentManagement","btnSave"]);
		}else{
		kony.automation.button.click(["frmConsentManagement","customHeader","btnRight"]);
		}
		// :End User Injected Code Snippet {78c1cf0c-1cfe-3f99-cbb1-452de59d143c}
		kony.automation.playback.wait(5000);
	}
	
	function ValidateSelectedConsent(){
		kony.automation.playback.waitFor(["frmConsentManagement","segConsentTypes"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmConsentManagement","segConsentTypes[0,0]","lblValue"], "text")).toEqual("Yes"); 
	}
	
	function OpenConsentManagement(){
		kony.automation.playback.waitFor(["frmSettings","segConsentManagement"],15000);
		kony.automation.segmentedui.click(["frmSettings","segConsentManagement[0,0]"]);
		kony.automation.playback.waitFor(["frmConsentManagement","segConsentTypes"],15000);
	}
	
	function setDefaultAccounts(){
	
	  kony.automation.playback.waitFor(["frmSettings","segSettingsDefaultAccount"],15000);
	  kony.automation.scrollToWidget(["frmSettings","segSettingsDefaultAccount"]);
	  kony.automation.segmentedui.scrollToRow(["frmSettings","segSettingsDefaultAccount[0,1]"]);
	  kony.automation.segmentedui.click(["frmSettings","segSettingsDefaultAccount[0,1]"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmSetDefaultAccount","segSelectAccounts"],15000);
	  kony.automation.segmentedui.click(["frmSetDefaultAccount","segSelectAccounts[0,0]"]);
	
	  kony.automation.playback.waitFor(["frmPreferencesDefaultAccount","segAccounts"],15000);
	  var accounts_Size=kony.automation.widget.getWidgetProperty(["frmPreferencesDefaultAccount","segAccounts"],"data");
	  var segLength=accounts_Size.length;
	  //isCheck=kony.automation.widget.getWidgetProperty(["frmPreferencesDefaultAccount","segAccounts[1,0]","imgRadio"]);
	
	  for(var x = 0; x <segLength; x++) {
	
	    var seg="segAccounts[0,"+x+"]";
	    var isCheck=kony.automation.widget.getWidgetProperty(["frmPreferencesDefaultAccount",seg,"imgRadio"],"text");
	
	    if(isCheck){
	      kony.print("Already set as default account");
	    }else{
	      kony.automation.segmentedui.click(["frmPreferencesDefaultAccount",seg]);
	      kony.automation.playback.wait(10000);
	      break;
	    }
	  }
	
	  kony.automation.playback.waitFor(["frmSetDefaultAccount","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmSetDefaultAccount","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function setAccountAlerts(){
	
	  kony.automation.playback.waitFor(["frmSettings","segSettingsAlerts"],15000);
	  kony.automation.segmentedui.click(["frmSettings","segSettingsAlerts[0,2]"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAlertsAccountPref","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmAlertsAccountPref","segTransactions[0,0]"]);
	  kony.automation.playback.wait(5000);
	
	  //kony.automation.playback.waitFor(["frmAlertGroupsList","lblInlineMessage"],15000);
	  //var isDisable=kony.automation.widget.getWidgetProperty(["frmAlertGroupsList","lblInlineMessage"],"text");
	
	  kony.automation.playback.waitFor(["frmAlertGroupsList","switchReceiveAlerts"],15000);
	  kony.automation.switch.toggle(["frmAlertGroupsList","switchReceiveAlerts"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.alert.click(0);
	  kony.automation.playback.wait(10000);
	
	  kony.automation.playback.waitFor(["frmAlertGroupsList","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAlertGroupsList","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAlertsAccountPref","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAlertsAccountPref","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyOnClickProfileName(){
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxHeaderMain"],15000);
	  kony.automation.widget.touch(["frmUnifiedDashboard","Hamburger","flxHeaderMain"], null,null,[92,28]);
	  kony.automation.playback.wait(10000);
	}
	
	function NavigateToProfileSettings(){
	  
	  kony.automation.playback.waitFor(["frmSettings","segSettingsProfile"],15000);
	  kony.automation.segmentedui.click(["frmSettings","segSettingsProfile[0,1]"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmProfilePersonalDetails","customHeader","btnRight"]);
	}
	function verifyAddingNewPhoneNumber(isPrimary,MobileNumber){
	
	  NavigateToProfileSettings();
	  
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","flxEditPhoneNumbers"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","flxEditPhoneNumbers"]);
	  var addNewNumber=kony.automation.playback.waitFor(["frmProfileEditPhoneNumbers","btnContinue"],15000);
	
	  if(addNewNumber){
	    kony.automation.button.click(["frmProfileEditPhoneNumbers","btnContinue"]);
	    kony.automation.playback.waitFor(["frmProfileSelectLocation","segContactLocation"],15000);
	    kony.automation.segmentedui.click(["frmProfileSelectLocation","segContactLocation[0,0]"]);
	    kony.automation.playback.waitFor(["frmProfileContactType","segContactType"],15000);
	    kony.automation.segmentedui.click(["frmProfileContactType","segContactType[0,1]"]);
	
	    kony.automation.playback.waitFor(["frmProfileEditPhoneNumberMain","keypad","btnEight"],15000);
	    for(i=0; i<MobileNumber.length; i++){
	      kony.automation.button.click(["frmProfileEditPhoneNumberMain","keypad", getBtnID(MobileNumber.charAt(i))]);
	    }
	
	    if(isPrimary==='YES'){
	      kony.automation.playback.waitFor(["frmProfileEditPhoneNumberMain","flxCheckboxPrimary"],15000);
	      kony.automation.flexcontainer.click(["frmProfileEditPhoneNumberMain","flxCheckboxPrimary"]);
	    }
	
	    kony.automation.playback.waitFor(["frmProfileEditPhoneNumberMain","btnVerifyPhoneNumber"],15000);
	    kony.automation.button.click(["frmProfileEditPhoneNumberMain","btnVerifyPhoneNumber"]);
	    kony.automation.playback.waitFor(["frmProfileConfirmDetails","btnUpdateChanges"],15000);
	    kony.automation.button.click(["frmProfileConfirmDetails","btnUpdateChanges"]);
	
	  }
	  
	  kony.automation.playback.waitFor(["frmProfileEditPhoneNumbers","customHeader","flxBack"]);
	  kony.automation.flexcontainer.click(["frmProfileEditPhoneNumbers","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","flxBack"]);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","customHeader","flxBack"]);
	}
	
	function verifyUpdatingPhoneNumber(){
	
	  kony.automation.playback.waitFor(["frmSettings","segSettingsProfile"],15000);
	  kony.automation.segmentedui.click(["frmSettings","segSettingsProfile[0,1]"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmProfilePersonalDetails","customHeader","btnRight"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","flxEditPhoneNumbers"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","flxEditPhoneNumbers"]);
	
	  kony.automation.playback.waitFor(["frmProfileEditPhoneNumbers","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfileEditPhoneNumbers","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","customHeader","flxBack"]);
	}
	
	function verifyAddingNewEmailAddress(isPrimary,emailAddress){
	
	  NavigateToProfileSettings();
	  
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","flxEditEmail"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","flxEditEmail"]);
	  var addNewEmail=kony.automation.playback.waitFor(["frmProfileEditEmails","btnContinue"],15000);
	
	  if(addNewEmail){
	    kony.automation.button.click(["frmProfileEditEmails","btnContinue"]);
	    kony.automation.playback.waitFor(["frmProfileEnterEmailID","tbxEmail"],15000);
	    kony.automation.textbox.enterText(["frmProfileEnterEmailID","tbxEmail"],emailAddress);
	    if(isPrimary==='YES'){
	      kony.automation.playback.waitFor(["frmProfileEnterEmailID","flxCheckboxPrimary"],15000);
	      kony.automation.flexcontainer.click(["frmProfileEnterEmailID","flxCheckboxPrimary"]);
	    }
	    kony.automation.playback.waitFor(["frmProfileEnterEmailID","btnContinue"],15000);
	    kony.automation.button.click(["frmProfileEnterEmailID","btnContinue"]);
	  }
	
	  kony.automation.playback.waitFor(["frmProfileEditEmails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfileEditEmails","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","customHeader","flxBack"]);
	}
	
	function verifyUpdatingEmailaddress(){
	
	  NavigateToProfileSettings();
	
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","flxEditEmail"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","flxEditEmail"]);
	
	  kony.automation.playback.waitFor(["frmProfileEditEmails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfileEditEmails","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","customHeader","flxBack"]);
	
	}
	
	
	function verifyAddingNewAddress(isPrimary,Address1,City,Pincode){
	
	  NavigateToProfileSettings();
	
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","flxEditAddress"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","flxEditAddress"]);
	
	  var addNewAddress=kony.automation.playback.waitFor(["frmProfileEditAddressList","btnContinue"],15000);
	  if(addNewAddress){
	    kony.automation.button.click(["frmProfileEditAddressList","btnContinue"]);
	    kony.automation.playback.waitFor(["frmProfileAddAddress","flxMainContainer","txtResidentialAddressLineOne"],15000);
	    kony.automation.playback.wait(1000);
	    kony.automation.textbox.enterText(["frmProfileAddAddress","flxMainContainer","txtResidentialAddressLineOne"],Address1);
	    kony.automation.playback.waitFor(["frmProfileAddAddress","flxMainContainer","lstbxCountry"],15000);
	    kony.automation.listbox.selectItem(["frmProfileAddAddress","flxMainContainer","lstbxCountry"], "IN");
	    kony.automation.playback.waitFor(["frmProfileAddAddress","flxMainContainer","lstbxState"],15000);
	    kony.automation.listbox.selectItem(["frmProfileAddAddress","flxMainContainer","lstbxState"], "IN-AP");
	    kony.automation.playback.waitFor(["frmProfileAddAddress","flxMainContainer","txtResidentialAddressCity"],15000);
	    kony.automation.textbox.enterText(["frmProfileAddAddress","flxMainContainer","txtResidentialAddressCity"],City);
	    kony.automation.playback.waitFor(["frmProfileAddAddress","flxMainContainer","txtResidentialAddressZipCode"],15000);
	    kony.automation.textbox.enterText(["frmProfileAddAddress","flxMainContainer","txtResidentialAddressZipCode"],Pincode);
	    kony.automation.playback.waitFor(["frmProfileAddAddress","flxMainContainer","btnContinueResidentialAddress"],15000);
	    kony.automation.button.click(["frmProfileAddAddress","flxMainContainer","btnContinueResidentialAddress"]);
	    kony.automation.playback.waitFor(["frmProfileAdressType","segAddressType"],15000);
	    kony.automation.button.click(["frmProfileAdressType","segAddressType[0,0]","btnOption"]);
	
	    if(isPrimary==="YES"){
	      kony.automation.playback.waitFor(["frmProfileConfirmAddressDetails","flxCheckboxPrimary"],15000);
	      kony.automation.flexcontainer.click(["frmProfileConfirmAddressDetails","flxCheckboxPrimary"]);
	    }
	
	    kony.automation.playback.waitFor(["frmProfileConfirmAddressDetails","btnUpdateChanges"],15000);
	    kony.automation.button.click(["frmProfileConfirmAddressDetails","btnUpdateChanges"]);
	  }
	
	  kony.automation.playback.waitFor(["frmProfileEditAddressList","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfileEditAddressList","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","customHeader","flxBack"]);
	
	}
	
	
	function verifyUpdatingaddress(){
	
	  NavigateToProfileSettings();
	
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","flxEditAddress"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","flxEditAddress"]);
	
	  kony.automation.playback.waitFor(["frmProfileEditAddressList","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfileEditAddressList","customHeader","flxBack"]);
	  kony.automation.playback.waitFor(["frmProfilePersonalDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmProfilePersonalDetails","customHeader","flxBack"]);
	
	}
	
	function enableDisableEStatement(){
		kony.automation.playback.waitFor(["frmSettings","segSettingsDefaultAccount"],15000);
		kony.automation.segmentedui.click(["frmSettings","segSettingsDefaultAccount[0,0]"]);
		kony.automation.playback.waitFor(["frmEStmtAccountPreferences","segSelectAccounts"],15000);
		kony.automation.segmentedui.click(["frmEStmtAccountPreferences","segSelectAccounts[0,0]"]);
		let btnDisable = kony.automation.playback.waitFor(["frmEStmtAccountDetails","btnDisable"],5000);
		
		if(btnDisable){
		kony.automation.button.click(["frmEStmtAccountDetails","btnDisable"]);
		kony.automation.playback.waitFor(["frmEStmtDisableEStatements","btnDisable"],15000);
		kony.automation.button.click(["frmEStmtDisableEStatements","btnDisable"]);
		}else{
		kony.automation.playback.waitFor(["frmEStmtAccountDetails","btnEnable"],5000);
		kony.automation.button.click(["frmEStmtAccountDetails","btnEnable"]);
		kony.automation.playback.waitFor(["frmEStmtEnableEStatements","flxCheckBox"],15000);
		kony.automation.widget.touch(["frmEStmtEnableEStatements","flxCheckBox"], [13,6],null,null);
		kony.automation.button.click(["frmEStmtEnableEStatements","btnEnable"]);
		}
		kony.automation.playback.waitFor(["frmEStmtAccountDetails","customPopup","lblPopup"],30000);
		expect(kony.automation.widget.getWidgetProperty(["frmEStmtAccountDetails","customPopup","lblPopup"], "text")).toContain("success");
		
		kony.automation.playback.waitFor(["frmEStmtAccountDetails","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEStmtAccountDetails","customHeader","flxBack"]);
		kony.automation.playback.waitFor(["frmEStmtAccountPreferences","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEStmtAccountPreferences","customHeader","flxBack"]);
	}
	function setDefault_PIN(pin){
		kony.automation.playback.waitFor(["frmSettings","segSettingsLogin"],15000);
		kony.automation.segmentedui.click(["frmSettings","segSettingsLogin[0,1]"]);
		kony.automation.playback.waitFor(["frmPreferencesDefaultLogin","flxOption3"],15000);
		kony.automation.flexcontainer.click(["frmPreferencesDefaultLogin","flxOption3"]);
		
		let keyPad = kony.automation.playback.waitFor(["frmDevRegPin","keypad","btnOne"],5000);
		if(keyPad){
			for(let i=0; i<pin.length; i++){
				kony.automation.button.click(["frmDevRegPin","keypad",getBtnID(pin.charAt(i))]);
			}
			kony.automation.button.click(["frmDevRegPin","btnNext"]);
			for(let i=0; i<pin.length; i++){
				kony.automation.button.click(["frmDevRegPin","keypad",getBtnID(pin.charAt(i))]);
			}
			kony.automation.button.click(["frmDevRegPin","btnEnable"]);
		}
		kony.automation.playback.waitFor(["frmPreferencesPin","btnSetAsDefault"],15000);
		kony.automation.button.click(["frmPreferencesPin","btnSetAsDefault"]);
		kony.automation.playback.waitFor(["frmPreferencesDefaultLogin","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmPreferencesDefaultLogin","customHeader","flxBack"]);
	}
	
	function loginWithPin(pin){
		
		kony.automation.playback.waitFor(["frmLogin","loginPopups","btnOne"],15000);
		for(let i=0; i<pin.length; i++){
				kony.automation.button.click(["frmLogin","loginPopups",getBtnID(pin.charAt(i))]);
		}
	    VerifyAccountsDashBoard();
	}
	
	function setDefault_Password() {
		kony.automation.playback.waitFor(["frmSettings","segSettingsLogin"],15000);
		kony.automation.segmentedui.click(["frmSettings","segSettingsLogin[0,1]"]);
		kony.automation.playback.waitFor(["frmPreferencesDefaultLogin","flxOption1"],15000);
		kony.automation.flexcontainer.click(["frmPreferencesDefaultLogin","flxOption1"]);
		kony.automation.playback.wait(3000);
		kony.automation.flexcontainer.click(["frmPreferencesDefaultLogin","customHeader","flxBack"]);
	}
	
	function enableDisableSecurityAlerts(){
		kony.automation.playback.waitFor(["frmSettings","segSettingsAlerts"],15000);
		kony.automation.segmentedui.click(["frmSettings","segSettingsAlerts[0,0]"]);
		kony.automation.playback.waitFor(["frmAlertGroupsList","switchReceiveAlerts"],15000);
		let selectedIndex = kony.automation.widget.getWidgetProperty(["frmAlertGroupsList","switchReceiveAlerts"], "selectedIndex");	
		kony.automation.switch.toggle(["frmAlertGroupsList","switchReceiveAlerts"]);
	
		if(selectedIndex === 0){
			kony.automation.alert.click(0);
		}
		kony.automation.playback.wait(10000);
		kony.automation.flexcontainer.click(["frmAlertGroupsList","customHeader","flxBack"]);
	}
	
	function getBtnID(num){
	  switch(num){
	    case '0' :
	      return "btnZero";
	    case '1' :
	      return "btnOne";
	    case '2' :
	      return "btnTwo";
	    case '3' :
	      return "btnThree";
	    case '4' :
	      return "btnFour";
	    case '5' :
	      return "btnFive";
	    case '6' :
	      return "btnSix";
	    case '7' :
	      return "btnSeven";
	    case '8' :
	      return "btnEight";
	    case '9' :
	      return "btnNine";
	  }
	}
	
	function getRandomNumber(length) {
	  var randomChars = '0123456789';
	  var result = '';
	  for ( var i = 0; i < length; i++ ) {
	    result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
	  }
	  return result;
	}
	
	it("VerifyBlockedFunds", async function() {
	  
	  ClickonFirstCheckingAccount();
	  verofyAdvancedSearch_BlockedFunds();
	  MoveBackfromAccountDetails();
	  VerifyAccountsDashBoard();
	  
	},120000);
	
	it("EnableDisableSecurityAlert", async function() {
	  NavigateToSettings();
	  enableDisableSecurityAlerts();
	  MoveBackFromSettings_DashBoard();
	},60000);
	
	it("VerifyAccountPreview", async function() {
	  
	  NavigateToSettings();
	  enableAccountPreview();
	  MoveBackFromSettings_DashBoard();
	  logout();
	  verifyPreview();
	});
	
	it("DefaultSignIn_PIN", async function() {
	  let pin = "123456";
	  NavigateToSettings();
	  setDefault_PIN(pin);
	  MoveBackFromSettings_DashBoard();
	  logout();
	  loginWithPin(pin);
	  NavigateToSettings();
	  setDefault_Password();
	  MoveBackFromSettings_DashBoard();
	});
});