describe("CardManagement", function() {
	function SelectAccountOndashBoard(account){
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","segAccounts"],15000);
	  let indices = getIndex(account);
	  kony.automation.widget.touch(["frmUnifiedDashboard","segAccounts"], null,null,[80,97]);
	  kony.automation.segmentedui.click(["frmUnifiedDashboard","segAccounts[" + indices.i + "," + indices.j +"]"]);
	  kony.automation.playback.wait(10000);
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","lblAccountName"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountSummaryNative","lblAccountName"], "text")).not.toBe('');
	
	}
	
	// function getIndex(account){
	//   let segData = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","segAccounts"], "data");
	//   let i=0;
	//   let j=0;
	
	//   for(var a = 0; a<segData.length; a++){
	//     for(var b=0; b<segData[a][1].length; b++){
	//       if(segData[a][1][b].accountName.includes(account)){
	//         i=a;
	//         j=b;
	//         break;
	//       }
	//     }
	//   }
	
	//   return {
	//     i,
	//     j
	//   };
	// }
	
	 function getIndex(account){
	  let segData = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","segAccounts"], "data");
	  let i=0;
	  let j=0;
	
	  for(var a = 0; a<segData.length; a++){
	    for(var b=0; b<segData[a][1].length; b++){
	      if(segData[a][1][b].accountName === undefined){
	        kony.automation.flexcontainer.click(["frmUnifiedDashboard","segAccounts[" + a + "," + b +"]","flxViewContainer"]);
	        kony.automation.playback.wait(2000);
	        return getIndex(account);                                   
	
	      }else{
	        if(segData[a][1][b].accountName.includes(account)){
	          i=a;
	          j=b;
	          break;
	        }                               
	      }
	    }
	  }
	  return {
	    i,
	    j
	  };
	}
	
	function ClickonFirstSavingAccount(){
	
	  SelectAccountOndashBoard(Transfers.savingsAccount.name);
	
	}
	
	function ClickonFirstCheckingAccount(){
	
	  SelectAccountOndashBoard(Transfers.checkingAccount.name);
	}
	
	function ClickonFirstLoanAccount(){
	
	  SelectAccountOndashBoard(Transfers.loanAccount.name);
	}
	
	function ClickonFirstCreditCardAccount(){
	
	  SelectAccountOndashBoard("Credit Card");
	}
	
	function ClickonFirstDepositAccount(){
	
	  SelectAccountOndashBoard(Transfers.depositAccount.name);
	}
	
	function initiateAndCancelTransfer(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
		kony.automation.playback.waitFor(["frmEuropeTransferToAccount","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEuropeTransferToAccount","customHeader","flxBack"]);
	}
	
	function initiateAndCancelPayment(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink3"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink3"]);
		kony.automation.playback.waitFor(["frmEuropeTransferToAccountSM","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmEuropeTransferToAccountSM","customHeader","flxBack"]);
	}
	
	function verifyCheckBookNavigation(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow9"]);
		kony.automation.playback.waitFor(["frmCMReview","customHeader","flxBack"]);
		kony.automation.widget.touch(["frmCMReview","customHeader","flxBack"], null,null,[27,39]);
	}
	
	function verifyNavigationToSettings(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow10"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow10"]);
		kony.automation.device.deviceBack();
	}
	
	function verifyNavigationViewStatement(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[53,36]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function VerifyAccInfo(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","customHeader","imgSearch"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","customHeader","imgSearch"], null,null,[11,9]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountInfo","information","lblTab1Header"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab1Header"], "text")).not.toBe('');
	}
	
	function VerifyPendingWithdrawal(){
		expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab1Field4Label"], "text")).toContain("Pending Withdrawals");  
	}
	
	function VerifyInterestDetails(){
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab2Field2Label"], "text")).toContain("Interest");
	}
	
	function VerifySwiftCode(){
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountInfo","information","lblTab3Field4Label"], "text")).toContain("SWIFT Code");
	}
	function MoveBackFromAccInfo(){
	
	  kony.automation.playback.waitFor(["frmAccountInfo","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccountInfo","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function verifyEditAccountNickname(NickName){
	
	  kony.automation.playback.waitFor(["frmAccountInfo","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmAccountInfo","customHeader","btnRight"]);
	  kony.automation.playback.waitFor(["frmAccountInfo","btnEditNickName"],15000);
	  kony.automation.button.click(["frmAccountInfo","btnEditNickName"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccInfoEdit","txtNickName"],15000);
	  kony.automation.textbox.enterText(["frmAccInfoEdit","txtNickName"],NickName);
	  kony.automation.playback.waitFor(["frmAccInfoEdit","btnSave"],15000);
	  kony.automation.button.click(["frmAccInfoEdit","btnSave"]);
	  kony.automation.playback.wait(5000);
	
	  MoveBackFromAccInfo();
	
	}
	function MoveBackfromAccountDetails(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	//   kony.automation.playback.waitFor(["frmAccountDetails","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.click(["frmAccountDetails","Hamburger","segHamburger[0,0]"]);
	//   kony.automation.playback.wait(5000);
	}
	
	function VerifyMoreoptionsDisplayed(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[57,28]);
	
	  kony.automation.playback.waitFor(["frmAccountDetails","flxCancel"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxCancel"]);
	}
	
	function VerifyOptionsOnLandingScreen(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink1"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink2"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink3"],15000);
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","lblLink4"],15000);
	
	  expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).not.toBe('');
	
	}
	
	function viewAccountStatement(){
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[62,46]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [305,78],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function viewLoanAccountStatement() {
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [192,98],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function viewDepositAccountStatement() {
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
		kony.automation.playback.waitFor(["frmAccStatements","flxSegStatements"],15000);
		kony.automation.widget.touch(["frmAccStatements","flxSegStatements"], [192,98],null,null);
		kony.automation.flexcontainer.click(["frmAccStatements","flxYearSelection"]);
		kony.automation.playback.waitFor(["frmSelectYear","segYears"],15000);
		kony.automation.segmentedui.click(["frmSelectYear","segYears[0,0]"]);
		kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	}
	
	function VerifyPendingTranscations(){
	
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	
	  try{
	    let isAvailable=kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,-1]","lblHeaderName"], "text");
	
	    kony.print("is Available "+isAvailable);
	
	    if(isAvailable.includes('Pending')){
	
	      kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,0]"]);
	      kony.automation.playback.wait(5000);
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],15000);
	      kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	      kony.automation.playback.wait(5000);
	
	      MoveBackfromAccountDetails();
	    }else{
	
	      MoveBackfromAccountDetails();
	    }
	  }catch(Exception){
	    MoveBackfromAccountDetails();
	  }
	
	  //   kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  //   kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[1,0]"]);
	
	  //   kony.automation.playback.wait(5000);
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.playback.wait(5000);
	}
	
	function VerifyPostedTranscations(){
	
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	
	  try{
	    let isAvailable=kony.automation.widget.getWidgetProperty(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,-1]","lblHeaderName"], "text");
	
	    kony.print("is Available "+isAvailable);
	
	    if(isAvailable.includes('Posted')){
	
	      kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,0]"]);
	      kony.automation.playback.wait(5000);
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	      kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],15000);
	      kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	      kony.automation.playback.wait(5000);
	
	      MoveBackfromAccountDetails();
	    }else{
	
	      MoveBackfromAccountDetails();
	    }
	  }catch(Exception){
	    MoveBackfromAccountDetails();
	  }
	
	  //   kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  //   kony.automation.segmentedui.click(["frmAccountDetails","accountsTransactionListNative","segTransactionsList[2,0]"]);
	
	  //   kony.automation.playback.wait(5000);
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","lblLocateUs"]);
	  //   expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  //   kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
	  //   kony.automation.playback.wait(5000);
	
	}
	
	function ScrollDownTranscations(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"],15000);
	  kony.automation.segmentedui.scrollToBottom(["frmAccountDetails","accountsTransactionListNative","segTransactionsList"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxAddRangeAmount"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","flxAddRangeAmount"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountFrom"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountFrom"],"1");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountTo"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountTo"],"100");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function verofyAdvancedSearch_BlockedFunds(){
		kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	  
		kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,1]"]);
		kony.automation.calendar.selectDate(["frmAdvanceSearch","calStartDate"], [3,1,2021]);
		kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
		
		kony.automation.playback.waitFor(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList"],10000);
		let noRecords = kony.automation.widget.getWidgetProperty(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList[0,0]","lblNoRecords"], "isVisible")
	
		if(!noRecords){
			kony.automation.segmentedui.click(["frmAdvanceSearchResults","accountsTransactionListNative","segTransactionsList[0,0]"]);
		kony.automation.playback.waitFor(["frmMMTransactionDetails","accountsTransactionDetailsNative","lblStatus"],10000);
		expect(kony.automation.widget.getWidgetProperty(["frmMMTransactionDetails","accountsTransactionDetailsNative","lblStatus"], "text")).toEqual("BLOCKED");
		kony.automation.playback.waitFor(["frmMMTransactionDetails","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmMMTransactionDetails","customHeader","flxBack"]);
		}
		
		kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
		kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	}
	
	function VerifyAdvancedSearch_ByAmount(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxAddRangeAmount"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","flxAddRangeAmount"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountFrom"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountFrom"],"1");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","txtAmountTo"],15000);
	  kony.automation.textbox.enterText(["frmAdvanceSearch","txtAmountTo"],"9999");
	  kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByDate(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTimeRangeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTimeRangeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTimeRange[0,5]"]);
	
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByTransactionType(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTransactionTypeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTransactionTypeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTransactionType[0,3]"]);
	  
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByDebitCredit(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","segType"],15000);
	  kony.automation.segmentedui.click(["frmAdvanceSearch","segType[0,0]"]);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","flxTransactionTypeWrapper"],15000);	
		kony.automation.flexcontainer.click(["frmAdvanceSearch","flxTransactionTypeWrapper"]);
	  kony.automation.playback.wait(3000);
		kony.automation.segmentedui.click(["frmAdvanceSearch","segTransactionType[0,0]"]);
	  
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifyAdvancedSearch_ByKeyword(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","accountSummaryNative","search","imgAdvancedSearchIcon"], [17,16],null,null);
	  kony.automation.playback.wait(5000);
	
	  kony.automation.playback.waitFor(["frmAdvanceSearch","tbxSearch"],15000);
		kony.automation.textbox.enterText(["frmAdvanceSearch","tbxSearch"],"Transfer");
	  kony.automation.playback.wait(3000);
	   kony.automation.playback.waitFor(["frmAdvanceSearch","btnSearch"],15000);
	  kony.automation.button.click(["frmAdvanceSearch","btnSearch"]);
	  kony.automation.playback.wait(5000);
	
	 kony.automation.playback.waitFor(["frmAdvanceSearchResults","customHeader","flxBack"],15000);
	  kony.automation.playback.wait(3000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearchResults","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmAdvanceSearch","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAdvanceSearch","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	}
	
	function VerifySavingsAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[54,27]);
	  kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow6"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyCheckingAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
	  kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[54,27]);
	  kony.automation.playback.waitFor(["frmAccountDetails","flxAddRow6"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow6"]);
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyCreditCardAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink3"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink3"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyLoanAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function VerifyDepositAccStatements(){
	
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
	  kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
	  kony.automation.playback.waitFor(["frmAccStatements","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmAccStatements","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function initiateTransfer(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink2"], "text")).toContain("Transfer");	
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink2"]);	
	}
	
	function verifyQuickLinkAccessForTransfer(){
	  kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink2"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink2"], "text")).toContain("Transfer");	
	}
	
	function createSavingsPotGoal(goalName){
	 /// kony.automation.playback.wait(4000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		
		if(kony.automation.playback.waitFor(["frmMySavingsPot","btnCreateSavingsPot1"],10000)){
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot1"]);
		}else{
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot"]);
		}
		
		kony.automation.playback.waitFor(["frmSavingsType","btnGoal"],15000);
		kony.automation.button.click(["frmSavingsType","btnGoal"]);
		kony.automation.playback.waitFor(["frmGoalsType","segGoalsType"],15000);
		kony.automation.segmentedui.click(["frmGoalsType","segGoalsType[0,0]"]);
		kony.automation.playback.waitFor(["frmGoalName","txtBox"],15000);
		kony.automation.textbox.enterText(["frmGoalName","txtBox"],goalName);
		kony.automation.button.click(["frmGoalName","btnContinue"]);
		kony.automation.playback.waitFor(["frmOptimizeGoal","flxAmountWrapper"],15000);
		kony.automation.flexcontainer.click(["frmOptimizeGoal","flxAmountWrapper"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnTwo"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnZero"]);
		kony.automation.button.click(["frmOptimizeGoal","keypad","btnZero"]);
		kony.automation.widget.touch(["frmOptimizeGoal","lblDone"], [24,16],null,null);
		kony.automation.slider.slide(["frmOptimizeGoal","SliderMonth"], 66);
		kony.automation.button.click(["frmOptimizeGoal","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateSavingsGoalFrequency","btnSave"],15000);
		kony.automation.button.click(["frmCreateSavingsGoalFrequency","btnSave"]);
		kony.automation.playback.waitFor(["frmCreateSavingsGoalFrequencyDate","customCalendar"],15000);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar"], null,null,[356,15]);
		kony.automation.flexcontainer.click(["frmCreateSavingsGoalFrequencyDate","customCalendar","flxNextMonth"]);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar"], null,null,[196,156]);
		kony.automation.widget.touch(["frmCreateSavingsGoalFrequencyDate","customCalendar","m3CopyLabel0a7f34907bda844"], null,null,[21,16]);
		kony.automation.button.click(["frmCreateSavingsGoalFrequencyDate","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateGoalVerifyDetails","btnContinue"],15000);
		kony.automation.button.click(["frmCreateGoalVerifyDetails","btnContinue"]);
		kony.automation.playback.waitFor(["frmGoalsAcknowledgement","btnFund"],15000);
		kony.automation.button.click(["frmGoalsAcknowledgement","btnFund"]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);
	}
	
	function createSavingsPotBudget(budgetName){
	  //kony.automation.playback.wait(4000);
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		
		if(kony.automation.playback.waitFor(["frmMySavingsPot","btnCreateSavingsPot1"],10000)){
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot1"]);
		}else{
			kony.automation.button.click(["frmMySavingsPot","btnCreateSavingsPot"]);
		}
		
		kony.automation.playback.waitFor(["frmSavingsType","btnBudget"],15000);
		kony.automation.button.click(["frmSavingsType","btnBudget"]);
		kony.automation.playback.waitFor(["frmBudgetName","txtBox"],15000);
		kony.automation.textbox.enterText(["frmBudgetName","txtBox"],budgetName);
		kony.automation.button.click(["frmBudgetName","btnContinue"]);
		kony.automation.playback.waitFor(["frmBudgetfundAmount","keypad","btnTwo"],15000);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnTwo"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","keypad","btnZero"]);
		kony.automation.button.click(["frmBudgetfundAmount","btnContinue"]);
		kony.automation.playback.waitFor(["frmCreateBudgetVerifyDetails","btnContinue"],15000);
		kony.automation.button.click(["frmCreateBudgetVerifyDetails","btnContinue"]);
		kony.automation.playback.waitFor(["frmBudgetAcknowledgement","lblSkip"],15000);
		kony.automation.widget.touch(["frmBudgetAcknowledgement","lblSkip"], null,null,[49,10]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);  
	}
	
	function updateSavingsPot(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","imgIcon1"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmAccountDetails","quicklinksNative","lblLink1"], "text")).toEqual("Savings Pot");
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink1"],15000);
		kony.automation.flexcontainer.click(["frmAccountDetails","quicklinksNative","flxLink1"]);
	
		kony.automation.playback.waitFor(["frmMySavingsPot","segMyGoals"],15000);
		kony.automation.segmentedui.click(["frmMySavingsPot","segMyGoals[0,0]"]);
		kony.automation.playback.waitFor(["frmSavingsGoalViewDetails","customHeader","btnRight"],15000);
		kony.automation.button.click(["frmSavingsGoalViewDetails","customHeader","btnRight"]);
		kony.automation.playback.waitFor(["frmEditSavingsGoal","btnSaveConfirm"],15000);
		kony.automation.button.click(["frmEditSavingsGoal","btnSaveConfirm"]);
		kony.automation.playback.waitFor(["frmEditGoalsAcknowledgement","btnFund"],15000);
		kony.automation.button.click(["frmEditGoalsAcknowledgement","btnFund"]);
		kony.automation.playback.waitFor(["frmMySavingsPot","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmMySavingsPot","customHeader","flxBack"]);
	}
	
	
	function clickDisputedTransactions(){
		kony.automation.playback.waitFor(["frmAccountDetails","quicklinksNative","flxLink4"],15000);
		kony.automation.widget.touch(["frmAccountDetails","quicklinksNative","flxLink4"], null,null,[42,25]);
		kony.automation.flexcontainer.click(["frmAccountDetails","flxAddRow7"]);
	}
	
	function selectDisputedTransaction(){
		kony.automation.playback.waitFor(["frmDisputedTransactionsList","segAccounts"],15000);
		kony.automation.segmentedui.click(["frmDisputedTransactionsList","segAccounts[0,0]"]);
	}
	
	function clickOnSendMessageButton(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","btnSendMessage"],15000);
		kony.automation.button.click(["frmDisputeTransactionDetails","btnSendMessage"]);
	}
	
	
	function verifyTrasactionDetails_Dispute(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","lblReferenceValue"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmDisputeTransactionDetails","lblReferenceValue"], "text")).not.toBeNull();
	}
	
	function goBackFromTransactionDetails_Dispute(){
		kony.automation.playback.waitFor(["frmDisputeTransactionDetails","customHeader","flxBack"],10000);
		kony.automation.flexcontainer.click(["frmDisputeTransactionDetails","customHeader","flxBack"]);
	}
	
	function goBackFromDisputeTransactions(){
		kony.automation.playback.waitFor(["frmDisputedTransactionsList","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmDisputedTransactionsList","customHeader","flxBack"]);
	}
	
	function VerifyAccountsDashBoard() {
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard"],30000);
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","lblSelectedAccountType"],30000);
	  expect(kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","lblSelectedAccountType"], "text")).not.toBe('');
	}
	
	function VerifySwipeOperationOnDashBoard(){
	
	  //kony.automation.scrollToWidget(["frmUnifiedDashboard","lblBarTitle"]);
	  kony.automation.scrollToWidget(["frmUnifiedDashboard","lblNetWorthSummary"]);
	}
	
	function VerifyNotchOperationOnDashBoard(){
	
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","flxInnerChartSizeToggle"],15000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","flxInnerChartSizeToggle"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","flxInnerChartSizeToggle"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function NavigateToViewAllTranscations() {
	
	  // Scroll to View All form
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","btnViewTransactionsGraph"],15000);
	  kony.automation.scrollToWidget(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
	  kony.automation.button.click(["frmUnifiedDashboard","btnViewTransactionsGraph"]);
	}
	
	function SelectUncategorizedTranscations(){
	
	  // Select Uncategorized Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","flxDropdownImage"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","flxDropdownImage"]);
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactionTypes"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactionTypes[0,1]"]);
	}
	
	function SelectCategorizedTranscations(){
	
	  // Select Categorized Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","flxDropdownImage"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","flxDropdownImage"]);
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactionTypes"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactionTypes[0,0]"]);
	}
	
	function VerifySearchFunctionality_ViewAllTranscation(){
	
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","tbxSearch"],15000);
	  kony.automation.textbox.enterText(["frmPFMCategorisedTransactions","tbxSearch"],"Spent");
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactions"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMCategorisedTransactions","segTransactions[0,0]","lblTransaction"], "text")).toContain("Spent");
	}
	
	function MoveBackfromViewAllTranscations(){
	
	  //MoveBack from viewAll Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","customHeader","flxBack"]);
	
	  VerifyAccountsDashBoard();
	}
	
	function MoveBackfromViewAllTranscations_MyMoney(){
	
	  //MoveBack from viewAll Transcations
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMCategorisedTransactions","customHeader","flxBack"]);
	
	}
	
	function VerifyTranscationDetails(){
	
	  kony.automation.playback.waitFor(["frmPFMCategorisedTransactions","segTransactions"],15000);
	  kony.automation.segmentedui.click(["frmPFMCategorisedTransactions","segTransactions[0,0]"]);
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","lblLocateUs"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","customHeader","lblLocateUs"], "text")).not.toBe('');
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblTransferredToTrans"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblTransferredToTrans"], "text")).not.toBe('');
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","lblTransferredFromTrans"],15000);
	  expect(kony.automation.widget.getWidgetProperty(["frmPFMTransactionDetails","lblTransferredFromTrans"], "text")).not.toBe('');
	}
	
	function MoveBackFromTranscationDetails(){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","customHeader","flxBack"]);
	  MoveBackfromViewAllTranscations();
	  VerifyAccountsDashBoard();
	}
	
	function MoveBackFromTranscationDetails_MyMoney(){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","flxBack"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","customHeader","flxBack"]);
	  MoveBackfromViewAllTranscations_MyMoney();
	}
	
	function EditTranscationDetails(Note){
	
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmPFMTransactionDetails","customHeader","btnRight"]);
	  kony.automation.playback.waitFor(["frmPFMTransactionDetails","flxOption2"],15000);
	  kony.automation.flexcontainer.click(["frmPFMTransactionDetails","flxOption2"]);
	  kony.automation.playback.waitFor(["frmPFMNote","txtNote"],15000);
	  kony.automation.textarea.enterText(["frmPFMNote","txtNote"],Note);
	  kony.automation.playback.waitFor(["frmPFMNote","btnSave"],15000);
	  kony.automation.button.click(["frmPFMNote","btnSave"]);
	
	}
	
	function VerifyAccountsOrder(){
	
	
	}
	
	function openMenu(menu){
	  kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"]);
	  var menuOptions = kony.automation.widget.getWidgetProperty(["frmUnifiedDashboard","Hamburger","segHamburger"], "data");
	  kony.print("menuOptions: "+menuOptions);
	  var menuIndex = -1;
	  for(i=0; i<menuOptions.length; i++){
	    if(menuOptions[i].text === menu){
	      menuIndex = i;
	      break;
	    }
	  }
	  if(menuIndex > -1){
	    kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0," + menuIndex+ "]" ]);
	    kony.automation.playback.wait(10000);
	  }else{
	    expect(menuIndex).toBeGreaterThan(-1);
	  }
	}
	
	
	
	
	function lockCard(){
		if(isCardUnlocked()){	
	      kony.automation.switch.toggle(["frmCardManageHome","switchActiveorInactive"]);
	      kony.automation.playback.waitFor(["frmTermsAndCondition","flxCheckBox"],15000);
	      kony.automation.flexcontainer.click(["frmTermsAndCondition","flxCheckBox"]);
	      kony.automation.button.click(["frmTermsAndCondition","btnContinue"]);
	      kony.automation.playback.waitFor(["frmCardManageHome","customPopup","lblPopup"], 15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmCardManageHome","customPopup","lblPopup"], "text")).toContain("success");
		}
	}
	
	function goToDashboardFromCardManagement(){
		kony.automation.playback.waitFor(["frmCardManageHome","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmCardManageHome","customHeader","flxBack"]);
		kony.automation.segmentedui.click(["frmCardManageHome","Hamburger","segHamburger[0,0]"]);
		
		VerifyAccountsDashBoard();
	}
	
	function goTocardManagement(){
	  openMenu("Card Management");
	}
	
	function unlockCard(){
		
	  if(!isCardUnlocked()){	
	      kony.automation.switch.toggle(["frmCardManageHome","switchActiveorInactive"]);
	      kony.automation.playback.waitFor(["frmCardManageHome","customPopup","lblPopup"], 15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmCardManageHome","customPopup","lblPopup"], "text")).toContain("success");
		}
	}
	
	function isCardUnlocked(){
		kony.automation.playback.waitFor(["frmCardManageHome","switchActiveorInactive"],15000);	
		let selectedIndex = kony.automation.widget.getWidgetProperty(["frmCardManageHome","switchActiveorInactive"] , "selectedIndex");
		
		return (selectedIndex === 1) ? true : false;
	}
	
	function changePin(){
	  if(isCardUnlocked()){
	    kony.automation.playback.waitFor(["frmCardManageHome","flxChangePin"],15000);
		kony.automation.flexcontainer.click(["frmCardManageHome","flxChangePin"]);
		kony.automation.playback.waitFor(["frmCardMngNewPin","txtCurrentPinValue"],15000);
		kony.automation.textbox.enterText(["frmCardMngNewPin","txtCurrentPinValue"],"4040");
		kony.automation.textbox.enterText(["frmCardMngNewPin","txtNewPin"],"0404");
		kony.automation.textbox.enterText(["frmCardMngNewPin","txtConfirmPin"],"0404");
		kony.automation.button.click(["frmCardMngNewPin","btnContinue"]);
		kony.automation.playback.waitFor(["frmCardManageHome","customPopup","lblPopup"], 15000);
		expect(kony.automation.widget.getWidgetProperty(["frmCardManageHome","customPopup","lblPopup"], "text")).toContain("success");
	  }else{
	    expect("Crad is locked ").toEqual("Please unlock the card first to change pin.");
	  }
	}
	
	function setPurchaseLimit(){
		if(isCardUnlocked()){
	      kony.automation.playback.waitFor(["frmCardManageHome","flxSetPurchaseLimit"],15000);
	      kony.automation.flexcontainer.click(["frmCardManageHome","flxSetPurchaseLimit"]);
	      let btnMinus = kony.automation.playback.waitFor(["frmSetPurchaseCardLimit","btnMinus"],5000);
	      if(btnMinus){
	          kony.automation.button.click(["frmSetPurchaseCardLimit","btnMinus"]);
	      }else{
	          kony.automation.button.click(["frmSetPurchaseCardLimit","btnPlus"]);
	      }
	      kony.automation.button.click(["frmSetPurchaseCardLimit","btnConfirm"]);
	      kony.automation.playback.waitFor(["frmSetCardLimitConfirmation","lblUpdateMsg"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmSetCardLimitConfirmation","lblUpdateMsg"], "text")).toContain("updated");
	      kony.automation.playback.waitFor(["frmSetCardLimitConfirmation","btnCardsManagement"],15000);
	      kony.automation.button.click(["frmSetCardLimitConfirmation","btnCardsManagement"]);
	    }else{
	      expect("Crad is locked ").toEqual("Please unlock the card first to set purchase limit.");
	    }
	}
	
	function setATMWithdrawalLimit(){
		if(isCardUnlocked()){
	      kony.automation.playback.waitFor(["frmCardManageHome","flxSetATMWithdrawalLimit"],15000);
	      kony.automation.flexcontainer.click(["frmCardManageHome","flxSetATMWithdrawalLimit"]);
	      let btnMinus = kony.automation.playback.waitFor(["frmSetWithdrawalCardLimit","btnMinus"],5000);
	      if(btnMinus){
	          kony.automation.button.click(["frmSetWithdrawalCardLimit","btnMinus"]);
	      }else{
	          kony.automation.button.click(["frmSetWithdrawalCardLimit","btnPlus"]);
	      }
	
	      kony.automation.button.click(["frmSetWithdrawalCardLimit","btnConfirm"]);
	      kony.automation.playback.waitFor(["frmSetCardLimitConfirmation","lblUpdateMsg"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmSetCardLimitConfirmation","lblUpdateMsg"], "text")).toContain("updated");
	      kony.automation.playback.waitFor(["frmSetCardLimitConfirmation","btnCardsManagement"],15000);
	      kony.automation.button.click(["frmSetCardLimitConfirmation","btnCardsManagement"]);
	    }else{
	      expect("Crad is locked ").toEqual("Please unlock the card first to set purchase limit.");
	    }
	}
	
	function verifyCardNoLastDigits(){
		kony.automation.playback.waitFor(["frmCardManageHome","lblCardNoLastDigits"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmCardManageHome","lblCardNoLastDigits"], "text")).not.toContain("X");  
	}
	
	function selectTravalDates(){
		kony.automation.playback.waitFor(["frmManageTravelStartDate","customCalendar","flxNextMonth"],15000);
		kony.automation.flexcontainer.click(["frmManageTravelStartDate","customCalendar","flxNextMonth"]);
		kony.automation.widget.touch(["frmManageTravelStartDate","customCalendar","m3CopyLabel0j8ef8b8e650148"], null,null,[20,11]);
		kony.automation.playback.waitFor(["frmManageTravelStartDate","btnContinue"],15000);
		kony.automation.button.click(["frmManageTravelStartDate","btnContinue"]);
		kony.automation.playback.waitFor(["frmManageTravelEndDate","customCalendar","flxNextMonth"],15000);
		kony.automation.flexcontainer.click(["frmManageTravelEndDate","customCalendar","flxNextMonth"]);
		kony.automation.playback.waitFor(["frmManageTravelEndDate","customCalendar","flxNextMonth"],15000);
		kony.automation.flexcontainer.click(["frmManageTravelEndDate","customCalendar","flxNextMonth"]);	 
		kony.automation.widget.touch(["frmManageTravelEndDate","customCalendar","m3CopyLabel0e9e5d9d7b7e84d"], null,null,[20,11]); 
		kony.automation.playback.waitFor(["frmManageTravelEndDate","btnContinue"],15000);
		kony.automation.button.click(["frmManageTravelEndDate","btnContinue"]);	
	}
	
	function openManageTravelPlans(){
		kony.automation.playback.waitFor(["frmCardManageHome","btnManageTravelPlans"],15000);
		kony.automation.button.click(["frmCardManageHome","btnManageTravelPlans"]);
	}
	
	function clickOnAddNewTravelPlan(){
		kony.automation.playback.waitFor(["frmManageTravelPlans","btnAddTravelPlans"],15000);
		kony.automation.button.click(["frmManageTravelPlans","btnAddTravelPlans"]);
	}
	
	
	function enterDestination(destination){
		kony.automation.playback.waitFor(["frmManageTravelDestination","tbxSearch"],15000);
		kony.automation.textbox.enterText(["frmManageTravelDestination","tbxSearch"],destination);
		kony.automation.playback.wait(1000);
		kony.automation.segmentedui.click(["frmManageTravelDestination","segTravelDestinationResults[0,0]"]);
		kony.automation.playback.wait(1000);
		kony.automation.button.click(["frmManageTravelDestination","btnAdd"]);
		kony.automation.button.click(["frmManageTravelDestination","btnSave"]);
	}
	
	function selectFirstCard(){
		kony.automation.playback.waitFor(["frmManageTravelSelectCards","segSelectCards"],15000);
		kony.automation.segmentedui.click(["frmManageTravelSelectCards","segSelectCards[0,0]"]);
		kony.automation.button.click(["frmManageTravelSelectCards","btnContinue"]);
		kony.automation.playback.waitFor(["frmManageTravelPhoneNumber","btnSkip"],15000);
		kony.automation.button.click(["frmManageTravelPhoneNumber","btnSkip"]);
	}
	
	function confirmAddTravelPlan(){
		kony.automation.playback.waitFor(["frmManageTravelConfirmation","btnConfirm"],15000);
		kony.automation.button.click(["frmManageTravelConfirmation","btnConfirm"]);
		kony.automation.playback.waitFor(["frmManageTravelPlans","customPopup","lblPopup"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmManageTravelPlans","customPopup","lblPopup"], "text")).toContain("success");
	}
	
	function goBackFromManageTravelPlans(){
		kony.automation.playback.waitFor(["frmManageTravelPlans","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmManageTravelPlans","customHeader","flxBack"]);
	}
	
	function deleteTravelPlan(){
		let segTravelPlans = kony.automation.playback.waitFor(["frmManageTravelPlans","segTravelPlans"],2000);
		
	    if(segTravelPlans){
	      kony.automation.playback.waitFor(["frmManageTravelPlans","segTravelPlans"],15000);
	      kony.automation.segmentedui.click(["frmManageTravelPlans","segTravelPlans[0,0]"]);
	      kony.automation.playback.waitFor(["frmManageTravelDetails","btnDelete"],15000);
	      kony.automation.button.click(["frmManageTravelDetails","btnDelete"]);
	      kony.automation.alert.click(0);
	      kony.automation.playback.waitFor(["frmManageTravelPlans","customPopup","lblPopup"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmManageTravelPlans","customPopup","lblPopup"], "text")).toContain("success");
		}
		else{
	      expect("No travel plans are added.").toBe("Please add a travel plan first");
		}  
	}
	
	function editTravelDestination(){
		let segTravelPlans = kony.automation.playback.waitFor(["frmManageTravelPlans","segTravelPlans"],15000);
		
	    if(segTravelPlans){
		kony.automation.playback.waitFor(["frmManageTravelPlans","segTravelPlans"],15000);
		kony.automation.segmentedui.click(["frmManageTravelPlans","segTravelPlans[0,0]"]);
		kony.automation.playback.waitFor(["frmManageTravelDetails","customHeader","btnRight"],15000);
		kony.automation.button.click(["frmManageTravelDetails","customHeader","btnRight"]);
		kony.automation.flexcontainer.click(["frmManageTravelDetails","flxEditTravelDestination"]); 
	    }
		else{
	      expect("No travel plans are added.").toBe("Please add a travel plan first");
	    }
	}
	
	function goBackFromTravelDetails_To_ManageTravelPlans(){
		kony.automation.playback.waitFor(["frmManageTravelDetails","customHeader","flxBack"],15000);
		kony.automation.flexcontainer.click(["frmManageTravelDetails","customHeader","flxBack"]);
	}
	
	function viewTransactions(){
		kony.automation.playback.waitFor(["frmCardManageHome","btnTransactionTab"],15000);
		kony.automation.button.click(["frmCardManageHome","btnTransactionTab"]);
		kony.automation.playback.waitFor(["frmCardManageHome","lblCreditAmount"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmCardManageHome","lblCreditAmount"], "text")).not.toBeNull();
		kony.automation.playback.wait(3000);
		kony.automation.button.click(["frmCardManageHome","btnManageTab"]);
	}
	
	function verifyCardDetails(){
	  
	    if(isCardUnlocked()){
	      kony.automation.playback.waitFor(["frmCardManageHome","flxCardDetails"],15000);
	      kony.automation.flexcontainer.click(["frmCardManageHome","flxCardDetails"]);
	      kony.automation.playback.waitFor(["frmCardManageDetails","lblCardNoValue"],15000);
	      expect(kony.automation.widget.getWidgetProperty(["frmCardManageDetails","lblCardNoValue"], "text")).not.toBeNull();
	      expect(kony.automation.widget.getWidgetProperty(["frmCardManageDetails","segCardDetails[0,0]","lblValue"], "text")).not.toBeNull();
	      expect(kony.automation.widget.getWidgetProperty(["frmCardManageDetails","segCardDetails[0,2]","lblValue"], "text")).not.toBeNull();
	      expect(kony.automation.widget.getWidgetProperty(["frmCardManageDetails","segCardDetails[0,5]","lblValue"], "text")).not.toBeNull();
	      kony.automation.playback.wait(1000);
	      kony.automation.playback.waitFor(["frmCardManageDetails","customHeader","flxBack"],15000);
	      kony.automation.flexcontainer.click(["frmCardManageDetails","customHeader","flxBack"]);
	    }
		else{
	       expect("Crad is locked ").toEqual("Please unlock the card first.");
	    }
	}
	
	function noCardsPresent(){
		kony.automation.playback.waitFor(["frmCardManageHome","lblNoCards"],10000);
		let lblNoCards = kony.automation.widget.getWidgetProperty(["frmCardManageHome","lblNoCards"], "text");
		return lblNoCards.includes("Apply Now.");
	}
	
	function clickToApplyCard(){
		if(noCardsPresent()){
			kony.automation.playback.waitFor(["frmCardManageHome","btnApplyCard"],15000);
			kony.automation.button.click(["frmCardManageHome","btnApplyCard"]);
	    }
	  else{
	    kony.automation.playback.waitFor(["frmCardManageHome","customHeader","flxSearch"]);
		kony.automation.flexcontainer.click(["frmCardManageHome","customHeader","flxSearch"]);
		kony.automation.button.click(["frmCardManageHome","btnApplyForCard"]);
	  }
	}
	
	function searchAndSelectAccount(accountName){
		kony.automation.playback.waitFor(["frmManageNewCardAccounts","tbxSearch"],15000);
		kony.automation.textbox.enterText(["frmManageNewCardAccounts","tbxSearch"],accountName);
		kony.automation.playback.wait(2000);	
		kony.automation.segmentedui.click(["frmManageNewCardAccounts","segTransactions[0,0]"]);
	}
	
	function selectCard(){
		kony.automation.playback.waitFor(["frmManageSelectNewCards","segSelectProducts"],15000);
		kony.automation.button.click(["frmManageSelectNewCards","segSelectProducts[0,0]","btnApply"]);
	}
	
	function enterNameOnCard(name){
		kony.automation.playback.waitFor(["frmManageNewCardName","txtNewPassword"],15000);
		kony.automation.textbox.enterText(["frmManageNewCardName","txtNewPassword"],name);
		kony.automation.button.click(["frmManageNewCardName","btnUpdatePassword"]);
	}
	
	function confirmToContinue(){
		kony.automation.playback.waitFor(["frmManageNewCardReview","btnTransfer"],15000);
		kony.automation.button.click(["frmManageNewCardReview","btnTransfer"]);
	}
	
	function enterCardPin(){
		kony.automation.playback.wait(4000);
		let currForm = kony.automation.getCurrentForm();
		kony.automation.playback.waitFor([currForm,"keypad","btnOne"]);
		kony.automation.button.click([currForm,"keypad","btnOne"]);
		kony.automation.button.click([currForm,"keypad","btnTwo"]);
		kony.automation.button.click([currForm,"keypad","btnThree"]);
		kony.automation.button.click([currForm,"keypad","btnFour"]);
		kony.automation.button.click([currForm,"btnNext"]);
	}
	
	function backToCardManagement(){
		kony.automation.playback.waitFor(["frmManageNewCardAck","btnDashboard"],15000);
		kony.automation.button.click(["frmManageNewCardAck","btnDashboard"]);
	}
	
	
	function getLast4DigitOfCardNumber(){
		kony.automation.playback.waitFor(["frmCardManageHome","lblCardNoLastDigits"],15000);
		return kony.automation.widget.getWidgetProperty(["frmCardManageHome","lblCardNoLastDigits"], "text");
	}
	
	function clickOnActivateCard(){
		kony.automation.playback.waitFor(["frmCardManageHome","btnActivateCard"],15000);
		kony.automation.button.click(["frmCardManageHome","btnActivateCard"]);
	}
	
	function enterCVV(cvv){
		kony.automation.playback.waitFor(["frmCardManageNewCVV","keypad","btnSix"],15000);
	    for(let i=0;i<cvv.length; i++){
	      kony.automation.button.click(["frmCardManageNewCVV","keypad",getBtnID(cvv.charAt(i))]);
	    }
		kony.automation.button.click(["frmCardManageNewCVV","btnContinue"]);
	}
	
	function verifySuccessMsg(){
		kony.automation.playback.waitFor(["frmCardManageAck","lblSuccessMessage"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmCardManageAck","lblSuccessMessage"], "text")).toContain("activated");
	}
	
	function goBackToCardManagement(){
		kony.automation.playback.waitFor(["frmCardManageAck","btnMyAccounts"],15000);
		kony.automation.button.click(["frmCardManageAck","btnMyAccounts"]);
	}
	
	function filterCards(){
		kony.automation.playback.waitFor(["frmCardManageHome","customHeader","flxSearch"],15000);
		kony.automation.flexcontainer.click(["frmCardManageHome","customHeader","flxSearch"]);
		kony.automation.button.click(["frmCardManageHome","btnFilterCards"]);
	}
	
	function selectAllAccounts(){
		kony.automation.playback.waitFor(["frmManageFilterCards","segCardsList"],15000);
		let isSelectedAll = kony.automation.widget.getWidgetProperty(["frmManageFilterCards","segCardsList[0,0]","imgCheckbox"], "src").includes("remembermetick.png");
		if(!isSelectedAll){
			kony.automation.segmentedui.click(["frmManageFilterCards","segCardsList[0,0]"]);
		}
		kony.automation.button.click(["frmManageFilterCards","btnContinueSelectProducts"]);
	}
	
	
	function navigateToTransfers(){
	
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	//   kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	//   kony.automation.playback.wait(5000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.scrollToRow(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	//   kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	//   kony.automation.playback.wait(10000);
	  
	  openMenu("Transfers");
	  
	}
	
	function NavigateToPayements(){
	  openMenu("Send Money");
	}
	
	function SelectFromAccount(fromAccount){
	  kony.automation.playback.wait(5000);
	 let currFormName = kony.automation.getCurrentForm();
	
	  kony.automation.playback.waitFor([currFormName,"tbxSearch"],15000);
	  kony.automation.widget.touch([currFormName,"tbxSearch"], [232,14],null,null);
	  kony.automation.playback.waitFor([currFormName,"customSearchbox","tbxSearch"],15000);
	  kony.automation.textbox.enterText([currFormName,"customSearchbox","tbxSearch"],fromAccount);
	  kony.automation.playback.wait(3000);
	  expect(kony.automation.widget.getWidgetProperty([currFormName,"segTransactions[0,0]","lblAccountName"], "text")).not.toBe('');
	  kony.automation.playback.waitFor([currFormName,"segTransactions"],15000);
	  kony.automation.segmentedui.click([currFormName,"segTransactions[0,0]"]);
	  kony.automation.playback.wait(5000);
	
	}
	function ClickOnTransferToNewRecipient(){
	
		kony.automation.playback.waitFor(["frmEuropeTransferToAccountSM","btnNewAccount"],15000);
		kony.automation.button.click(["frmEuropeTransferToAccountSM","btnNewAccount"]);
		kony.automation.playback.waitFor(["frmEuropeTransferToAccountNewBen","SegSelectBank"],15000);
	}
	
	function SelectToAccount(ToAccReciptent){
		let currFormName = kony.automation.getCurrentForm();
	  kony.automation.playback.waitFor([currFormName,"tbxSearch"],15000);
	  kony.automation.widget.touch([currFormName,"tbxSearch"], [72,22],null,[72,65]);
	
	  if(ToAccReciptent==="OwnAcc"){
	    kony.automation.textbox.enterText([currFormName,"customSearchbox","tbxSearch"],"Saving");
	    kony.automation.playback.wait(3000);
	    kony.automation.playback.waitFor([currFormName,"segTransactions"],15000);
	    kony.automation.segmentedui.scrollToRow([currFormName,"segTransactions[0,0]"]);
	
	    expect(kony.automation.widget.getWidgetProperty([currFormName,"segTransactions[0,0]","lblAccountName"], "text")).not.toBe('');
	  }else{
	    kony.automation.textbox.enterText([currFormName,"customSearchbox","tbxSearch"],ToAccReciptent);
	    kony.automation.playback.wait(3000);
	    kony.automation.playback.waitFor([currFormName,"segTransactions"],15000);
	    kony.automation.segmentedui.scrollToRow([currFormName,"segTransactions[0,0]"]);
	
	    expect(kony.automation.widget.getWidgetProperty([currFormName,"segTransactions[0,0]","lblAccountName"], "text")).not.toBe('');
	  }  
	
	  kony.automation.playback.waitFor([currFormName,"segTransactions"],15000);
	  kony.automation.segmentedui.click([currFormName,"segTransactions[0,0]"]);
	  kony.automation.playback.wait(5000);
	}
	
	function SelectTransferAccount(ToAccReciptent){
	  kony.automation.playback.wait(4000);
	  let currFormName = kony.automation.getCurrentForm();
	  let index = {"i" : 0 , "j" : 0};
	  kony.automation.playback.waitFor([currFormName,"tbxSearch"],15000);
	  kony.automation.widget.touch([currFormName,"tbxSearch"], [72,22],null,[72,65]);
	
	  if(ToAccReciptent==="OwnAcc"){
	    kony.automation.textbox.enterText([currFormName,"customSearchbox","tbxSearch"],"Saving");
	    kony.automation.playback.wait(3000);
	    kony.automation.playback.waitFor([currFormName,"segTransactions"],15000);
	    
		let  segData = kony.automation.widget.getWidgetProperty([currFormName,"segTransactions"], "data");
		index = getIndexOfAccount(segData , "Saving");
	    kony.automation.segmentedui.scrollToRow([currFormName,"segTransactions["+index.i +"," + index.j + "]"]);
	
	    expect(kony.automation.widget.getWidgetProperty([currFormName,"segTransactions["+index.i +"," + index.j + "]","lblAccountName"], "text")).not.toBe('');
	  }else{
	    kony.automation.textbox.enterText([currFormName,"customSearchbox","tbxSearch"],ToAccReciptent);
	    kony.automation.playback.wait(3000);
	    kony.automation.playback.waitFor([currFormName,"segTransactions"],15000);
	    
	    let  segData = kony.automation.widget.getWidgetProperty([currFormName,"segTransactions"], "data");
	    index = getIndexOfAccount(segData , ToAccReciptent);
	    
	    kony.automation.segmentedui.scrollToRow([currFormName,"segTransactions["+index.i +"," + index.j + "]"]);
	
	    expect(kony.automation.widget.getWidgetProperty([currFormName,"segTransactions["+index.i +"," + index.j + "]","lblAccountName"], "text")).not.toBe('');
	  }  
	
	  kony.automation.playback.waitFor([currFormName,"segTransactions"],15000);
	  kony.automation.segmentedui.click([currFormName,"segTransactions["+index.i +"," + index.j + "]"]);
	  kony.automation.playback.wait(5000);
	}
	
	function EnterAmount(Amount) {
	  kony.automation.playback.wait(3000);
	let currFormName = kony.automation.getCurrentForm();
	  kony.automation.playback.waitFor([currFormName,"keypad","btnThree"],15000);
	  for(i=0; i<Amount.length; i++){
	    kony.automation.button.click([currFormName,"keypad", getBtnID(Amount.charAt(i))]);
	  }
	  kony.automation.playback.waitFor([currFormName,"btnContinue"],15000);
	  kony.automation.button.click([currFormName,"btnContinue"]);
	  kony.automation.playback.wait(5000);
	}
	
	function SelectFrequency(ValTimePeriod) {
	  // kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	
	  // kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	  
	  kony.automation.playback.waitFor(["frmEuropeVerifyTransferDetails","segDetails"],15000);
	  let segData = kony.automation.widget.getWidgetProperty(["frmEuropeVerifyTransferDetails","segDetails"], "data");
	  let index = getIndexFromSegment(segData , "Frequency");
	  kony.automation.segmentedui.click(["frmEuropeVerifyTransferDetails","segDetails[0,"+index+ "]"]);
	
	  kony.automation.playback.waitFor(["frmEuropeFrequency","segOptions"],15000);
	  switch(ValTimePeriod){
	    case "Daily":
	      kony.automation.segmentedui.click(["frmEuropeFrequency","segOptions[0,1]"]);
	      break;
	    case "Weekly":
	      kony.automation.segmentedui.click(["frmEuropeFrequency","segOptions[0,2]"]);
	      break;
	    case "HalfY":
	      kony.automation.segmentedui.click(["frmEuropeFrequency","segOptions[0,6]"]);
	      break;
	    case "Yearly":
	      kony.automation.segmentedui.click(["frmEuropeFrequency","segOptions[0,7]"]);
	      break;
	    case "QTR":
	      kony.automation.segmentedui.click(["frmEuropeFrequency","segOptions[0,5]"]);
	      break;
	    case "Monthly":
	      kony.automation.segmentedui.click(["frmEuropeFrequency","segOptions[0,4]"]);
	      break;
	  }
	
	  kony.automation.playback.wait(5000);
	}
	
	
	function SelectDateRange() {
	
	  kony.automation.playback.waitFor(["frmEuropeDuration","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmEuropeDuration","segOptions[0,0]"]);
	
	  kony.automation.playback.waitFor(["frmEuropeStartDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmEuropeStartDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//Select "Start" date - [1 lines]]
	  kony.automation.widget.touch(["frmEuropeStartDate","customCalendar","m3CopyLabel0j8ef8b8e650148"], null,null,[20,11]);
	  // :End User Injected Code Snippet {1b928f08-dcde-b4a4-8a34-66c9403263c8}
	  kony.automation.playback.waitFor(["frmEuropeEndDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmEuropeEndDate","customCalendar","flxNextMonth"]);
	  kony.automation.playback.waitFor(["frmEuropeEndDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmEuropeEndDate","customCalendar","flxNextMonth"]);
	  // :User Injected Code Snippet [//select "End" date - [3 lines]]
	  //kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0e9e5d9d7b7e84d"]);
	
	  kony.automation.widget.touch(["frmEuropeEndDate","customCalendar","m3CopyLabel0e9e5d9d7b7e84d"], null,null,[20,11]);  // :End User Injected Code Snippet {52f4e47a-c3c2-0551-1005-1c35e4be2ef7}
	  kony.automation.playback.waitFor(["frmEuropeEndDate","btnContinue"],15000);
	  kony.automation.button.click(["frmEuropeEndDate","btnContinue"]);
	
	  kony.automation.playback.wait(8000);
	}
	
	function SelectSendOnDate() {
	
	  kony.automation.playback.waitFor(["frmEuropeVerifyTransferDetails","segDetails"],15000);
	  let segData = kony.automation.widget.getWidgetProperty(["frmEuropeVerifyTransferDetails","segDetails"], "data");
	  let index = getIndexFromSegment(segData , "Send On");
	  kony.automation.segmentedui.click(["frmEuropeVerifyTransferDetails","segDetails[0,"+index+ "]"]);
	
	  // Selet Start date
	   kony.automation.playback.waitFor(["frmEuropeStartDate","customCalendar","flxNextMonth"],15000);
		kony.automation.flexcontainer.click(["frmEuropeStartDate","customCalendar","flxNextMonth"]);
	  kony.automation.widget.touch(["frmEuropeStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	
	  kony.automation.playback.waitFor(["frmEuropeStartDate","btnContinue"],15000);
	  kony.automation.button.click(["frmEuropeStartDate","btnContinue"]);
	  kony.automation.playback.wait(5000);
	}
	
	function SelectOccurences() {
	
	  kony.automation.playback.waitFor(["frmMMDuration","segOptions"],15000);
	  kony.automation.segmentedui.click(["frmMMDuration","segOptions[0,1]"]);
	
	  // Selet Start date
	  kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	  kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	  kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	
	  kony.automation.button.click(["frmMMNumberOfTransfers","keypad","btnTwo"]);
	  kony.automation.button.click(["frmMMNumberOfTransfers","btnContinue"]);
	  kony.automation.playback.wait(5000);
	
	}
	
	function EnterPaymentReference(notes) {
		kony.automation.playback.waitFor(["frmEuropeTransferReference","tbxReference"],15000);
		kony.automation.textbox.enterText(["frmEuropeTransferReference","tbxReference"],notes);
	  kony.automation.playback.waitFor(["frmEuropeTransferReference","btnContinue"],15000);
		kony.automation.button.click(["frmEuropeTransferReference","btnContinue"]);
		kony.automation.playback.wait(5000);
	}
	
	function SelectNormalDomesticPayment(){
		kony.automation.playback.waitFor(["frmEuropePaymentMedium","segOptions"],15000);
		kony.automation.segmentedui.click(["frmEuropePaymentMedium","segOptions[0,0]"]);
		kony.automation.playback.waitFor(["frmEuropeFeePayment","segOptions"],15000);
		kony.automation.segmentedui.click(["frmEuropeFeePayment","segOptions[0,0]"]);
		kony.automation.playback.wait(5000);
	}
	
	function SelectInstantDomesticPayment(){
		kony.automation.playback.waitFor(["frmEuropePaymentMedium","segOptions"],15000);
		kony.automation.segmentedui.click(["frmEuropePaymentMedium","segOptions[0,1]"]);
		kony.automation.playback.waitFor(["frmEuropeFeePayment","segOptions"],15000);
		kony.automation.segmentedui.click(["frmEuropeFeePayment","segOptions[0,0]"]);
		kony.automation.playback.wait(5000);
	}
	
	function SelectIWillPayFees(){
		kony.automation.playback.waitFor(["frmEuropeFeePayment","segOptions"],15000);
		kony.automation.segmentedui.click(["frmEuropeFeePayment","segOptions[0,0]"]);
		kony.automation.playback.wait(5000);
	}
	/**
	 * @function
	 *
	 */
	function ConfirmTransfer() {
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmEuropeVerifyTransferDetails","flxAddIcon"],15000);
	  kony.automation.flexcontainer.click(["frmEuropeVerifyTransferDetails","flxAddIcon"]);
	  kony.automation.playback.waitFor(["frmEuropeVerifyTransferDetails","btnClose"],10000);
	  kony.automation.button.click(["frmEuropeVerifyTransferDetails","btnClose"]);
	  kony.automation.playback.wait(5000);
	  kony.automation.playback.waitFor(["frmEuropeVerifyTransferDetails","btnTransfer"],25000);
	  kony.automation.button.click(["frmEuropeVerifyTransferDetails","btnTransfer"]);
	  kony.automation.playback.wait(5000);
	  
	
	}
	
	function CancelTransfer(){
	
	  kony.automation.playback.waitFor(["frmEuropeVerifyTransferDetails","customHeader","btnRight"],15000);
	  kony.automation.button.click(["frmEuropeVerifyTransferDetails","customHeader","btnRight"]);
	  kony.automation.playback.wait(5000);
	}
	
	function verifyBankNameInReviewScreen(fromAccount, toAccount){
		kony.automation.playback.waitFor(["frmEuropeVerifyTransferDetails","lblFromAccountValue"],15000);
	  
		fromAccount = fromAccount.substring(0,4);
		toAccount = toAccount.substring(0,4);
		
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeVerifyTransferDetails","lblFromAccountValue"], "text")).toContain(fromAccount);
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeVerifyTransferDetails","lblToAccountValue"], "text")).toContain(toAccount);
	}
	
	function verifyBankNameInAckScreen(fromAccount, toAccount){
		kony.automation.playback.wait(5000);
		kony.automation.playback.waitFor(["frmEuropeConfirmation","lblSuccessMessage"],15000);
	  
		fromAccount = fromAccount.substring(0,4);
		toAccount = toAccount.substring(0,4);
		
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","segDetails[0,1]","lblDetails"], "text")).toContain(fromAccount);
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","segDetails[0,2]","lblDetails"], "text")).toContain(toAccount);
	}
	
	function VerifyDataTruncated(){
		kony.automation.playback.waitFor(["frmEuropeConfirmation","segDetails"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","segDetails[0,0]","lblDetails"], "text")).not.toBeNull();
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","segDetails[0,1]","lblDetails"], "text")).not.toBeNull();
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","segDetails[0,2]","lblDetails"], "text")).not.toBeNull();
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","segDetails[0,3]","lblDetails"], "text")).not.toBeNull();
	}
	
	function VerifySwitftCode(){
		let swiftCode = kony.automation.playback.waitFor(["frmEuropeConfirmation_1","lblSwiftCodeabc"],2000);
		let address = kony.automation.playback.waitFor(["frmEuropeConfirmation","lblAddress"],2000);
		
		if(swiftCode && address){
		expect(true).toBe(false);
		}
	}
	
	function VerifyPaymentMethod(){
		let paymentMethod = kony.automation.playback.waitFor(["frmEuropeConfirmation","segDetails[0,3]","lblTitle"],5000);
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","segDetails[0,3]","lblTitle"], "text")).not.toEqual("Payment Method");
	}
	
	function VerifyTransferSuccessMessage() {
	
		kony.automation.playback.waitFor(["frmEuropeConfirmation","lblSuccessMessage"],15000);
		expect(kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","lblSuccessMessage"], "text")).not.toBe('');
	
		kony.automation.playback.waitFor(["frmEuropeConfirmation","flxError"],5000);
		let flxError = kony.automation.widget.getWidgetProperty(["frmEuropeConfirmation","flxError"], "isVisible");
		if(flxError){
	      expect("Transaction").toBe("successful");
	      kony.automation.button.click(["frmEuropeConfirmation","btnTryAgain"]);
	      kony.automation.playback.wait(1000);	
	      kony.automation.playback.waitFor(["frmEuropeTransferFromAccount","customHeader","btnRight"],15000);
	      kony.automation.button.click(["frmEuropeTransferFromAccount","customHeader","btnRight"]);
		}
		else{
	      kony.automation.playback.waitFor(["frmEuropeConfirmation","btnDashboard"],15000);
	      kony.automation.button.click(["frmEuropeConfirmation","btnDashboard"]);
	      kony.automation.playback.wait(5000);
	    }
		VerifyAccountsDashBoard();
	}
	
	function MoveBackToLandingScreen_Transfers(){
	
		kony.automation.playback.waitFor(["frmMMTransferAmount","customHeader","btnRight"],15000);
		kony.automation.button.click(["frmMMTransferAmount","customHeader","btnRight"]);
		kony.automation.playback.wait(5000);
	    VerifyAccountsDashBoard();
	}
	
	function getRandomString(length) {
	  var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	  var result = '';
	  for ( var i = 0; i < length; i++ ) {
	    result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
	  }
	  return result;
	}
	
	function getBtnID(num){
	  switch(num){
	    case '0' :
	      return "btnZero";
	    case '1' :
	      return "btnOne";
	    case '2' :
	      return "btnTwo";
	    case '3' :
	      return "btnThree";
	    case '4' :
	      return "btnFour";
	    case '5' :
	      return "btnFive";
	    case '6' :
	      return "btnSix";
	    case '7' :
	      return "btnSeven";
	    case '8' :
	      return "btnEight";
	    case '9' :
	      return "btnNine";
	  }
	}
	
	function getRandomNumber(length) {
	  var randomChars = '0123456789';
	  var result = '';
	  for ( var i = 0; i < length; i++ ) {
	    result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
	  }
	  return result;
	}
	
	function getIndexFromSegment(segData , val){
		for(let i=0; i < segData.length; i++){
			if(segData[i].property === val){
				return i;
			}
		}
		return 0; //default
	}
	
	function getIndexOfAccount(segData , accountName){
	  let i=0;
	  let j=0;
	  for(i=0; i<segData.length; i++){
	      if(segData[i].length > 1){
	          for(j=0; j<segData[i][1].length; j++){
	              if(segData[i][1][j].processedName.includes(accountName)){
	                  return { i, j};
	              }
	          }
	      }
	  }
	  return {i,j };
	}
	
	
	// Previous Functions- Commenting
	
	// function SearchInFromAndToScreen(valType) {
	
	//   try{
	
	
	//     var currentwidget1 = kony.automation.widget.getWidgetProperty(["frmLogout","btnLogIn"], "text");
	//     kony.print("The current Form Name ::"+currentwidget1);
	//     if(currentwidget1 === "Sign In"){
	
	//       kony.automation.button.click(["frmLogout","btnLogIn"]);
	//       kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
	//       kony.automation.textbox.enterText(["frmLogin","login","tbxUsername"],"dbxJasmine1234");
	//       kony.automation.textbox.enterText(["frmLogin","login","tbxPassword"],"Kony@1234");
	//       kony.automation.button.click(["frmLogin","login","btnLogIn"]);
	//       kony.automation.playback.wait(25000);
	//     }    
	
	//   }catch(err){
	//     kony.print("Unable to find widget");
	//   }
	//   //kony.automation.playback.wait(25000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	//   kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	//   kony.automation.playback.wait(5000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.scrollToRow(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	//   kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	//   kony.automation.playback.wait(8000);
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	//   kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], null,null,null);
	//   kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"check");
	//   //Kony.automation.playback.wait(5000);
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	//   //var checkingAcc = ;
	//   expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]","lblAccountName"], "text")).not.toBe(null);
	//   // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"]);
	//   kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	//   if(valType==="normal"){
	//     kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"credit");
	//   }else{
	//     kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"PToPRecipient");
	//   }  
	//   kony.automation.playback.wait(1000);
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	//   kony.automation.segmentedui.scrollToRow(["frmMMTransferToAccount","segTransactions[0,0]"]);
	//   var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]","lblAccountName"],"text");
	
	//   expect(creditCard).not.toBe(null);
	
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],10000);
	//   kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	// }
	
	
	// // function EnterAmount() {
	// //   kony.automation.playback.wait(5000);
	// //   kony.automation.playback.waitFor(["frmMMTransferAmount","keypad","btnThree"],10000);
	// //   kony.automation.button.click(["frmMMTransferAmount","keypad","btnThree"]);
	// //   kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	// //   kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	// //   kony.automation.button.click(["frmMMTransferAmount","btnContinue"]);
	// //   kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],10000);
	// // }
	
	
	// function SelectFrequencyOnceAndTransfer() {
	
	//   kony.automation.playback.waitFor(["frmMMReview","segDetails"],10000);
	//   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	//   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMStartDate","btnContinue"],15000);
	//   kony.automation.button.click(["frmMMStartDate","btnContinue"]);
	//   kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],15000);
	//   kony.automation.button.click(["frmMMReview","btnTransfer"]);
	//   expect(kony.automation.playback.waitFor(["frmMMConfirmation","lblSuccessMessage"],20000)).toEqual(true);
	//   kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	// }
	
	
	// function TransferScheduleOnce() {
	//   // :User Injected Code Snippet [//Prerequisite Verify_Search_In_FromAndToScreen - []]
	
	//   // :End User Injected Code Snippet {0d4c824b-a9ad-8c0e-5c44-a3292aac96a1}
	//   kony.automation.playback.waitFor(["frmMMTransferAmount","keypad","btnThree"],10000);
	//   kony.automation.button.click(["frmMMTransferAmount","keypad","btnThree"]);
	//   kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	//   kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	//   kony.automation.button.click(["frmMMTransferAmount","btnContinue"]);
	//   kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	//   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	//   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	//   // :User Injected Code Snippet [//Select a date - [3 lines]]
	//   kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxNextMonth"], [178,125],null,[178,125]);
	//   kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	//   // :End User Injected Code Snippet {414f83d0-e0a2-735c-c437-a564878872a6}
	//   kony.automation.playback.waitFor(["frmMMStartDate","btnContinue"],15000);
	//   kony.automation.button.click(["frmMMStartDate","btnContinue"]);
	//   kony.automation.playback.waitFor(["frmMMReview","btnTransfer"]);
	//   kony.automation.button.click(["frmMMReview","btnTransfer"]);
	//   expect(kony.automation.playback.waitFor(["frmMMConfirmation","btnToAccount"],10000)).toContain(true);
	//   kony.automation.button.click(["frmMMConfirmation","btnToAccount"]);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	// }
	
	// function SelectNumberOfTransfersAndThenConfirmTransfer(valTimePeriod,valueType) {
	//   kony.automation.playback.waitFor(["frmMMDuration","segOptions"],10000);
	//   kony.automation.segmentedui.click(["frmMMDuration","segOptions[0,1]"]);
	//   kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	//   // :User Injected Code Snippet [//Select a Start Date - [1 lines]]
	//   kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	//   // :End User Injected Code Snippet {83f58aab-137d-4051-a051-d2801329c71d}
	//   kony.automation.playback.waitFor(["frmMMNumberOfTransfers","keypad","btnFive"],15000);
	//   kony.automation.button.click(["frmMMNumberOfTransfers","keypad","btnFive"]);
	//   kony.automation.playback.waitFor(["frmMMNumberOfTransfers","btnContinue"],15000);
	//   kony.automation.button.click(["frmMMNumberOfTransfers","btnContinue"]);
	//   kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],15000);
	//   kony.automation.button.click(["frmMMReview","btnTransfer"]);
	//   expect(kony.automation.playback.waitFor(["frmMMConfirmation","btnDashboard"],10000)).toBe(true);
	//   kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	// }
	
	
	// function ScheduledTransferDailyDateRange() {
	//   kony.automation.playback.waitFor(["frmMMTransferAmount","keypad","btnThree"],10000);
	//   kony.automation.button.click(["frmMMTransferAmount","keypad","btnThree"]);
	//   kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	//   kony.automation.button.click(["frmMMTransferAmount","keypad","btnZero"]);
	//   kony.automation.playback.waitFor(["frmMMTransferAmount","btnContinue"],15000);
	//   kony.automation.button.click(["frmMMTransferAmount","btnContinue"]);
	//   kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	//   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	//   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,1]"]);
	//   kony.automation.playback.waitFor(["frmMMDuration","segOptions"],15000);
	//   kony.automation.segmentedui.click(["frmMMDuration","segOptions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	//   // :User Injected Code Snippet [//Select "Start" date - [1 lines]]
	//   kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	//   // :End User Injected Code Snippet {1b928f08-dcde-b4a4-8a34-66c9403263c8}
	//   kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	//   kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	//   // :User Injected Code Snippet [//select "End" date - [3 lines]]
	//   //kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"]);
	
	//   kony.automation.widget.touch(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	//   // :End User Injected Code Snippet {52f4e47a-c3c2-0551-1005-1c35e4be2ef7}
	//   kony.automation.playback.waitFor(["frmMMEndDate","btnContinue"],15000);
	//   kony.automation.button.click(["frmMMEndDate","btnContinue"]);
	//   kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],15000);
	//   kony.automation.button.click(["frmMMReview","btnTransfer"]);
	//   kony.automation.playback.waitFor(["frmMMConfirmation","btnDashboard"],15000);
	//   kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","lblLocateUs"],10000);
	// }
	
	
	
	// function SearchP2PInFromAndToScreen() {
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],10000);
	//   kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	//   kony.automation.playback.wait(2000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	//   kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], [100,13],null,[100,56]);
	//   kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"checki");
	//   kony.automation.playback.wait(1000);
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	//   //var checkingAcc = ;
	//   expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]","lblAccountName"], "text")).toContain("Check");
	//   // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"],15000);
	//   kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	//   kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"P2P");
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	//   // :User Injected Code Snippet [//Expect P2P recipient - [2 lines]]
	//   var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]","lblAccountName"],"text");
	//   expect(creditCard).not.toBe(null);
	//   // :End User Injected Code Snippet {2875156c-0fc0-2414-6f01-69c86f59b306}
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],10000);
	//   kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	// }
	
	// function SearchSameBankInFromAndToScreen() {
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],10000);
	//   kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	//   kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], [100,13],null,[100,56]);
	//   kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"checki");
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	//   //var checkingAcc = ;
	//   expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]","lblAccountName"], "text")).not.toBe(null);
	//   // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"],15000);
	//   kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	//   kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"samebank2");
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	//   // :User Injected Code Snippet [//Expect Credit Card - [2 lines]]
	//   var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]","lblAccountName"],"text");
	//   expect(creditCard).not.toBe(null);
	//   // :End User Injected Code Snippet {2875156c-0fc0-2414-6f01-69c86f59b306}
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],10000);
	//   kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	// }
	
	
	
	// function SelectDateRangeAndConfirmTransfer(ValTimePeriod, valueType) {
	//   //write your automation code here
	//   kony.automation.playback.wait(5000);
	//   kony.automation.playback.waitFor(["frmMMDuration","segOptions[0,0]"],10000);
	//   kony.automation.segmentedui.click(["frmMMDuration","segOptions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMStartDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.flexcontainer.click(["frmMMStartDate","customCalendar","flxNextMonth"]);
	//   // :User Injected Code Snippet [//Select "Start" date - [1 lines]]
	//   kony.automation.widget.touch(["frmMMStartDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	//   // :End User Injected Code Snippet {1b928f08-dcde-b4a4-8a34-66c9403263c8}
	//   kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.playback.wait(5000);
	//   kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	//   kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxNextMonth"],15000);
	//   kony.automation.flexcontainer.click(["frmMMEndDate","customCalendar","flxNextMonth"]);
	//   // :User Injected Code Snippet [//select "End" date - [3 lines]]
	//   // kony.automation.playback.waitFor(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"]);
	
	//   kony.automation.widget.touch(["frmMMEndDate","customCalendar","flxMonth","m3CopyLabel0ac5bc532de9c4c"], null,null,[17,17]);
	//   // :End User Injected Code Snippet {52f4e47a-c3c2-0551-1005-1c35e4be2ef7}
	//   kony.automation.playback.waitFor(["frmMMEndDate","btnContinue"],15000);
	//   kony.automation.button.click(["frmMMEndDate","btnContinue"]);
	//   kony.automation.playback.waitFor(["frmMMReview","btnTransfer"],10000);
	//   kony.automation.button.click(["frmMMReview","btnTransfer"]);
	//   try{
	//     kony.automation.playback.wait(10000);
	//     var errorMsg1 = kony.automation.widget.getWidgetProperty(["frmMMConfirmation","btnNewTransfer"], "text");
	
	//     if(errorMsg1==="New Transfer"){
	//       kony.print("Error::Inside New Transfer");
	//       kony.automation.playback.waitFor(["frmMMConfirmation","btnDashboard"],15000);
	//       kony.automation.button.click(["frmMMConfirmation","btnDashboard"]);
	
	//     }else{
	//       kony.print("***********************");
	//       //       var lblMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblMessage"], "text");
	//       // 		var lblSuccessMessage = kony.automation.widget.getWidgetProperty(["frmMMConfirmation", "lblSuccessMessage"], "text");
	
	//       // 		expect(lblMessage.toLowerCase()).toContain("success");
	//       // 		expect(lblSuccessMessage.toLowerCase()).toContain("success");
	
	//       var tempB=kony.automation.widget.getWidgetProperty(["frmMMConfirmation","lblMessage"], "text");
	//       kony.print("TeLL Me the Value :"+tempB);
	//       expect(tempB).toContain("Successfully Scheduled Transfer");
	//       kony.automation.playback.waitFor(["frmMMConfirmation","btnToAccount"],15000);
	//       kony.automation.button.click(["frmMMConfirmation","btnToAccount"]);
	//     }
	
	//   }catch(err){
	//     kony.print("Error::Unable to find element");
	
	//     try{
	//       kony.automation.playback.wait(10000);
	//       var currentwidget1 = kony.automation.widget.getWidgetProperty(["frmLogout","btnLogIn"], "text");
	//       kony.print("The current Form Name ::"+currentwidget1);
	//       if(currentwidget1 === "Sign In"){
	
	//         kony.automation.button.click(["frmLogout","btnLogIn"]);
	//         kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
	//         kony.automation.textbox.enterText(["frmLogin","login","tbxUsername"],"dbxJasmine1234");
	//         kony.automation.textbox.enterText(["frmLogin","login","tbxPassword"],"Kony@1234");
	//         kony.automation.button.click(["frmLogin","login","btnLogIn"]);
	//         kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],5000);
	//         /***********************************/
	
	//         //ValTimePeriod, ValueType
	//         SearchInFromAndToScreen(valueType);
	//         EnterAmount();
	//         SelectFrequency(ValTimePeriod);
	//         SelectDateRangeAndConfirmTransfer(ValTimePeriod, valueType);
	
	//       }     
	
	//     }catch(err1){
	//       kony.print("Error::"+err1.message);
	//     }
	//   }
	//   kony.automation.playback.wait(10000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	// }
	
	
	// // function SelectFrequencyDaily() {
	// //   kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	// //   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	// //   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	// //   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,1]"]);
	// // }
	
	// // function SelectFrequencyHalfYearly() {
	// //   kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	// //   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	// //   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	// //   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,6]"]);
	// // }
	
	
	// // function SelectFrequencyMonthly() {
	// //   kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	// //   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	// //   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	// //   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,4]"]);
	// // }
	
	// // function SelectFrequencyQuarterly() {
	// //   kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	// //   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	// //   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	// //   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,5]"]);
	// // }
	
	// // function SelectFrequencyWeekly() {
	// //   kony.automation.playback.waitFor(["frmMMReview","segDetails"]);
	// //   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	// //   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"]);
	// //   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,2]"]);
	// // }
	
	
	// // function SelectFrequencyYearly() {
	// //   kony.automation.playback.waitFor(["frmMMReview","segDetails"],15000);
	// //   kony.automation.segmentedui.click(["frmMMReview","segDetails[0,0]"]);
	// //   kony.automation.playback.waitFor(["frmMMFrequency","segOptions"],15000);
	// //   kony.automation.segmentedui.click(["frmMMFrequency","segOptions[0,7]"]);
	// // }
	
	// function OpenManageRecipientP2P() {
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	//   kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	//   kony.automation.playback.wait(2000);
	//   kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,4]"]);
	//   kony.automation.playback.waitFor(["frmManageRecipientType","segRecipientType"],15000);
	//   kony.automation.playback.wait(2000);
	//   kony.automation.segmentedui.click(["frmManageRecipientType","segRecipientType[1,0]"]);
	//   kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"],15000);
	// }
	
	
	// function SearchP2PInFromAndToScreen() {
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	//   kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	//   kony.automation.playback.wait(2000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","segHamburger"],15000);
	//   kony.automation.segmentedui.click(["frmUnifiedDashboard","Hamburger","segHamburger[0,1]"]);
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","tbxSearch"],15000);
	//   kony.automation.widget.touch(["frmMMTransferFromAccount","tbxSearch"], [100,13],null,[100,56]);
	//   kony.automation.textbox.enterText(["frmMMTransferFromAccount","customSearchbox","tbxSearch"],"checki");
	//   kony.automation.playback.wait(1000);
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   // :User Injected Code Snippet [//Expect Checking Account - [2 lines]]
	//   //var checkingAcc = ;
	//   expect(kony.automation.widget.getWidgetProperty(["frmMMTransferFromAccount","segTransactions[0,0]","lblAccountName"], "text")).not.toBe(null);
	//   // :End User Injected Code Snippet {391e3878-1a4a-6cd5-312f-c42e3dc249f3}
	//   kony.automation.playback.waitFor(["frmMMTransferFromAccount","segTransactions"],15000);
	//   kony.automation.segmentedui.click(["frmMMTransferFromAccount","segTransactions[0,0]"]);
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","tbxSearch"],15000);
	//   kony.automation.widget.touch(["frmMMTransferToAccount","tbxSearch"], [72,22],null,[72,65]);
	//   kony.automation.textbox.enterText(["frmMMTransferToAccount","customSearchbox","tbxSearch"],"P2P");
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	//   // :User Injected Code Snippet [//Expect P2P recipient - [2 lines]]
	//   var creditCard = kony.automation.widget.getWidgetProperty(["frmMMTransferToAccount","segTransactions[0,0]]","flxAccountsNoImageTransfers","flxMain","flxAccountName","lblAccountName"],"text");
	//   expect(creditCard).not.toBe(null);
	//   // :End User Injected Code Snippet {2875156c-0fc0-2414-6f01-69c86f59b306}
	//   kony.automation.playback.waitFor(["frmMMTransferToAccount","segTransactions"],15000);
	//   kony.automation.segmentedui.click(["frmMMTransferToAccount","segTransactions[0,0]"]);
	// }
	
	// function GoBackToDB() {
	//   kony.automation.playback.waitFor(["frmManageRecipientList","btnAddRecipient"],10000);
	//   kony.automation.playback.waitFor(["frmManageRecipientList","customHeader","flxBack"],15000);
	//   kony.automation.flexcontainer.click(["frmManageRecipientList","customHeader","flxBack"]);
	//   kony.automation.playback.waitFor(["frmManageRecipientType","customHeader","flxBack"],15000);
	//   kony.automation.flexcontainer.click(["frmManageRecipientType","customHeader","flxBack"]);
	
	// }
	
	// function Logout() {
	//   kony.automation.playback.wait(25000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	//   kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	//   kony.automation.playback.wait(3000);
	//   kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxLogout"],5000);
	//   kony.automation.widget.touch(["frmUnifiedDashboard","Hamburger","flxLogout"], null,null,[34,28]);
	//   kony.automation.playback.waitFor(["frmLogout","btnLogIn"],10000);
	//   kony.automation.button.click(["frmLogout","btnLogIn"]);
	//   kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],10000);
	// }
	
	it("ApplyNewCard", async function() {
	  goTocardManagement();
	  clickToApplyCard();
	  searchAndSelectAccount("Current Account");
	  selectCard();
	  enterNameOnCard("Classic Card");
	  confirmToContinue();
	  enterCardPin();
	  enterCardPin(); //re-enter
	  backToCardManagement();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("ActivateCard", async function() {
		goTocardManagement();
		let last4Digits = getLast4DigitOfCardNumber();
		let cvv = last4Digits.substr(1,3);
		clickOnActivateCard();
		enterCVV(cvv);
		verifySuccessMsg();
		goBackToCardManagement();
		goToDashboardFromCardManagement();
	},15000);
	
	it("FilterCards", async function() {
		goTocardManagement();
		filterCards();
		selectAllAccounts();
		goToDashboardFromCardManagement();
	},15000);
	
	it("AddTravelPlan", async function() {
	  goTocardManagement();
	  openManageTravelPlans();
	  clickOnAddNewTravelPlan();
	  selectTravalDates();
	  enterDestination("India");
	  selectFirstCard();
	  confirmAddTravelPlan();
	  goBackFromManageTravelPlans();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("EditTravelPlan", async function() {
	  goTocardManagement();
	  openManageTravelPlans();
	  editTravelDestination();
	  enterDestination("Australia");
	  goBackFromTravelDetails_To_ManageTravelPlans();
	  goBackFromManageTravelPlans();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("DeleteTravelPlan", async function() {
	  goTocardManagement();
	  openManageTravelPlans();
	  deleteTravelPlan();
	  goBackFromManageTravelPlans();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("ChangePIN", async function() {
	  goTocardManagement();
	  changePin();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("SetATMWithdrawalLimit", async function() {
	  goTocardManagement();
	  setATMWithdrawalLimit();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("SetPurchaseLimit", async function() {
	  goTocardManagement();
	  setPurchaseLimit();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("VerifyCardDetails", async function() {
	  goTocardManagement();
	  verifyCardDetails();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("VerifyCardNumber", async function() {
	  goTocardManagement();
	  verifyCardNoLastDigits();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("ViewTransactions", async function() {
	  goTocardManagement();
	  viewTransactions();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("LockCard", async function() {
	  goTocardManagement();
	  lockCard();
	  goToDashboardFromCardManagement();
	},120000);
	
	it("UnlockCard", async function() {
	  goTocardManagement();
	  unlockCard();
	  goToDashboardFromCardManagement();
	},120000);
});