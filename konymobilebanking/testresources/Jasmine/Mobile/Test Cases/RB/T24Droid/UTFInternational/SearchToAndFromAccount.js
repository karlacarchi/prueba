it("SearchToAndFromAccount", async function() {
  let fromAccount = "8648";
  let toAccount = "International";
  navigateToUTF();
  selectMakeTransferInternational();
  SelectTransferFromAccount(fromAccount);
  SelectTransferToAccount(toAccount);
  cancelEnterAmount();
  goToDashboardFromUTF();
});