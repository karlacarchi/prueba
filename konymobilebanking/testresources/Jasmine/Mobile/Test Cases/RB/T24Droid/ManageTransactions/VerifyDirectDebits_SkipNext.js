it("VerifyDirectDebits_SkipNext", async function() {
	openManageTransactions();
	selectDirectDebitTab();
	verifyDirectDebit();
	goBackFromDirectDebit();
	goBackFromDirectDebit();
	goBackFromTransfersTabs();
	VerifyAccountsDashBoard();
},180000);