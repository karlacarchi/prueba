it("CreateGoalPot", async function() {
	let goalName = "My Goal "+getRandomString(3);
	ClickonFirstCheckingAccount();
  createSavingsPotGoal(goalName);
  MoveBackfromAccountDetails();
  VerifyAccountsDashBoard();
},120000);