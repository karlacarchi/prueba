it("ActivateCard", async function() {
	goTocardManagement();
	let last4Digits = getLast4DigitOfCardNumber();
	let cvv = last4Digits.substr(1,3);
	clickOnActivateCard();
	enterCVV(cvv);
	verifySuccessMsg();
	goBackToCardManagement();
	goToDashboardFromCardManagement();
},15000);