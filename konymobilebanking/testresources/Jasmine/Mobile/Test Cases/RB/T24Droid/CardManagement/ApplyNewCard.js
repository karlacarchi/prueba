it("ApplyNewCard", async function() {
  goTocardManagement();
  clickToApplyCard();
  searchAndSelectAccount("Current Account");
  selectCard();
  enterNameOnCard("Classic Card");
  confirmToContinue();
  enterCardPin();
  enterCardPin(); //re-enter
  backToCardManagement();
  goToDashboardFromCardManagement();
},120000);