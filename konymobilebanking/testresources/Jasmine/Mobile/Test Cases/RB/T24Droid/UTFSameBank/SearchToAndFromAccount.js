it("SearchToAndFromAccount", async function() {
  let fromAccount = "8648";
  let toAccount = "Savings";
  navigateToUTF();
  selectMakeTransferWithinSameBank();
  SelectTransferFromAccount(fromAccount);
  SelectTransferToAccount(toAccount);
  cancelEnterAmount();
  goToDashboardFromUTF();
});