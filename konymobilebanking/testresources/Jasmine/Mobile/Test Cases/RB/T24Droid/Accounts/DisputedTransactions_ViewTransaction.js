it("DisputedTransactions_ViewTransaction", async function() {
  
  ClickonFirstCheckingAccount();
	clickDisputedTransactions();
	try{
		selectDisputedTransaction();
		verifyTrasactionDetails_Dispute();
		goBackFromTransactionDetails_Dispute();
		goBackFromDisputeTransactions();
	}catch(exception){
	//No records available
		goBackFromTransactionDetails_Dispute();
	}
		MoveBackfromAccountDetails();
		VerifyAccountsDashBoard();
},120000);