it("DisputedTransactions_SendMessage", async function() {
  
  ClickonFirstCheckingAccount();
	clickDisputedTransactions();
	try{
		selectDisputedTransaction();
		clickOnSendMessageButton();
		ComposeNewMessage_DisputedTransaction();
		MoveBackToDashBoard_Messages();
	}catch(exception){
	//No records available
		MoveBackfromAccountDetails();
		VerifyAccountsDashBoard();
	}
},120000);