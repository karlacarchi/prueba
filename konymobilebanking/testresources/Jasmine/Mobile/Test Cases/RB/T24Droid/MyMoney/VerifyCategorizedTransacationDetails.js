it("VerifyCategorizedTransacationDetails", async function() {
	goToMyMoney();
	ClickonViewAllTranxButton();
  SelectCategorizedTranscations();
  VerifySearchFunctionality_ViewAllTranscation();
  VerifyTranscationDetails();
  MoveBackFromTranscationDetails_MyMoney();
  goBackToDashboard_MyMoney();

},120000);