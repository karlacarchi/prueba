it("DefaultSignIn_PIN", async function() {
  let pin = "123456";
  NavigateToSettings();
  setDefault_PIN(pin);
  MoveBackFromSettings_DashBoard();
  logout();
  loginWithPin(pin);
  NavigateToSettings();
  setDefault_Password();
  MoveBackFromSettings_DashBoard();
});