function sendFeedback(){
	kony.automation.playback.waitFor(["frmInAppFeedbackRating","imgStar4"],15000);
	kony.automation.flexcontainer.click(["frmInAppFeedbackRating","flxStar4"]);
	kony.automation.playback.wait(2000);
	kony.automation.textarea.enterText(["frmInAppFeedbackRating","textarea"],"Optional comment");
	kony.automation.button.click(["frmInAppFeedbackRating","btnSubmit"]);
	kony.automation.playback.waitFor(["frmFeedBackSuccess","btnDone"],15000);
	kony.automation.button.click(["frmFeedBackSuccess","btnDone"]);
	kony.automation.playback.waitFor(["frmUnifiedDashboard","flxDashboard","segAccounts"],15000);
}