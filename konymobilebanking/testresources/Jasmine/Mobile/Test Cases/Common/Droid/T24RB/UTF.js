function navigateToUTF(){
  openMenu("Unified Transfers flow");
}

function selectMakeTransferWithinSameBank() {
	kony.automation.playback.waitFor(["frmSelectTransferType","transferTypeSelection1","btnAction1"],15000);
	kony.automation.playback.wait(3000);
	kony.automation.button.click(["frmSelectTransferType","transferTypeSelection1","btnAction1"]);
}

function selectMakeTransferDomesticAccount(){
	kony.automation.playback.waitFor(["frmSelectTransferType","transferTypeSelection2","btnAction1"],15000);
	kony.automation.playback.wait(3000);
	kony.automation.button.click(["frmSelectTransferType","transferTypeSelection2","btnAction1"]);  
}

function selectMakeTransferInternational(){
	kony.automation.playback.waitFor(["frmSelectTransferType","transferTypeSelection3","btnAction1"],15000);
	kony.automation.playback.wait(3000);
	kony.automation.button.click(["frmSelectTransferType","transferTypeSelection3","btnAction1"]);  
}

function SelectTransferFromAccount(ToAccReciptent){
  kony.automation.playback.wait(4000);
  let currFormName = kony.automation.getCurrentForm();
  let index = {"i" : 0 , "j" : 0};

	kony.automation.playback.waitFor([currFormName,"MakeATransfer","tbxFromSearch"],15000);
	kony.automation.textbox.enterText([currFormName,"MakeATransfer","tbxFromSearch"],ToAccReciptent);
	kony.automation.playback.waitFor([currFormName,"MakeATransfer","segFromAccounts"],15000);

	let  segData = kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segFromAccounts"], "data");
  index = getIndexOfAccount_UTF(segData , ToAccReciptent);
  kony.automation.segmentedui.scrollToRow([currFormName,"MakeATransfer","segFromAccounts["+index.i +"," + index.j + "]"]);

  expect(kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segFromAccounts["+index.i +"," + index.j + "]","lblField1"], "text")).not.toBe('');
  kony.automation.playback.waitFor([currFormName,"MakeATransfer","segFromAccounts"],15000);
  kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFromAccounts["+index.i +"," + index.j + "]"]);

}

function SelectTransferToAccount(ToAccReciptent){
  kony.automation.playback.wait(4000);
  let currFormName = kony.automation.getCurrentForm();
  let index = {"i" : 0 , "j" : 0};

	kony.automation.playback.waitFor([currFormName,"MakeATransfer","tbxToSearch"],15000);
	kony.automation.textbox.enterText([currFormName,"MakeATransfer","tbxToSearch"],ToAccReciptent);
	kony.automation.playback.waitFor([currFormName,"MakeATransfer","segToAccounts"],15000);

	let  segData = kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segToAccounts"], "data");
  index = getIndexOfAccount_UTF(segData , ToAccReciptent);
  kony.automation.segmentedui.scrollToRow([currFormName,"MakeATransfer","segToAccounts["+index.i +"," + index.j + "]"]);

  expect(kony.automation.widget.getWidgetProperty([currFormName,"MakeATransfer","segToAccounts["+index.i +"," + index.j + "]","lblField1"], "text")).not.toBe('');
  kony.automation.playback.waitFor([currFormName,"MakeATransfer","segToAccounts"],15000);
  kony.automation.segmentedui.click([currFormName,"MakeATransfer","segToAccounts["+index.i +"," + index.j + "]"]);

}

function enterAmount(Amount) {
  kony.automation.playback.wait(3000);
  let currFormName = kony.automation.getCurrentForm();
  kony.automation.playback.waitFor([currFormName,"MakeATransfer","btnThree"],15000);
  for(i=0; i<Amount.length; i++){
    kony.automation.button.click([currFormName,"MakeATransfer", getBtnID(Amount.charAt(i))]);
  }
  kony.automation.playback.waitFor([currFormName,"MakeATransfer", "btnContinue"],3000);
  kony.automation.button.click([currFormName,"MakeATransfer","btnContinue"]);
  kony.automation.playback.wait(5000);
}

function selectFrequency(ValTimePeriod) {
   kony.automation.playback.wait(5000);
  let currFormName = kony.automation.getCurrentForm();

	kony.automation.playback.waitFor([currFormName,"MakeATransfer","flxField4"],15000);
	kony.automation.flexcontainer.click([currFormName,"MakeATransfer","flxField4"]);
	kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,1]"]);

  kony.automation.playback.waitFor([currFormName,"MakeATransfer","segFrequencyOptions"],15000);
  switch(ValTimePeriod){
    case "Daily":
      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,1]"]);
      break;
    case "Weekly":
      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,2]"]);
      break;
    case "HalfY":
      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,6]"]);
      break;
    case "Yearly":
      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,7]"]);
      break;
    case "QTR":
      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,5]"]);
      break;
    case "Monthly":
      kony.automation.segmentedui.click([currFormName,"MakeATransfer","segFrequencyOptions[0,4]"]);
      break;
  }

  kony.automation.playback.wait(5000);
}

function confirmTransfer(){
	kony.automation.playback.wait(5000);
	let currFormName = kony.automation.getCurrentForm();
	kony.automation.playback.waitFor([currFormName,"MakeATransfer","imgAddIcon"],15000);
	kony.automation.widget.touch([currFormName,"MakeATransfer","imgAddIcon"], [7,11],null,null);
	kony.automation.playback.waitFor([currFormName,"MakeATransfer","flxSelectOptionsCancel"],15000);
	kony.automation.widget.touch([currFormName,"MakeATransfer","flxSelectOptionsCancel"], [117,38],null,null);
	kony.automation.playback.waitFor([currFormName,"MakeATransfer","btnTransfer"],15000);
	kony.automation.button.click([currFormName,"MakeATransfer","btnTransfer"]);
}

function verifyTransferSuccessMessage() {
	
	kony.automation.playback.waitFor(["flxSameBankAcknowledgement","Acknowledgement","lblSuccess"],10000);
	let currFormName = kony.automation.getCurrentForm();	
  expect(kony.automation.widget.getWidgetProperty([currFormName,"Acknowledgement","lblSuccess"], "text")).not.toBe('');
	kony.automation.playback.waitFor([currFormName,"Acknowledgement","flxFail"],5000);
	let flxError = kony.automation.widget.getWidgetProperty([currFormName,"Acknowledgement","flxFail"], "isVisible");
	if(flxError){
      expect("Transaction").toBe("successful");
      kony.automation.button.click([currFormName, "Acknowledgement","btnFailureAction1"]);
      kony.automation.playback.wait(1000);	
      // kony.automation.playback.waitFor(["frmEuropeTransferFromAccount","customHeader","btnRight"],15000);
      // kony.automation.button.click(["frmEuropeTransferFromAccount","customHeader","btnRight"]);
	}
	else{
		kony.automation.playback.waitFor([currFormName,"Acknowledgement","btnSuccessAction1"],15000);
		kony.automation.button.click([currFormName,"Acknowledgement","btnSuccessAction1"]);
		kony.automation.playback.waitFor(["frmSelectTransferType","imgBack"],15000);
		kony.automation.widget.touch(["frmSelectTransferType","imgBack"], [14,13],null,null);
		kony.automation.playback.wait(5000);
    }
	VerifyAccountsDashBoard();
}

function goToDashboardFromUTF(){
	kony.automation.playback.waitFor(["frmSelectTransferType","imgBack"],15000);
	kony.automation.widget.touch(["frmSelectTransferType","imgBack"], [19,16],null,null);
	let flag = kony.automation.playback.waitFor(["frmSelectTransferType","imgBack"],3000);
	if(flag){
		kony.automation.widget.touch(["frmSelectTransferType","imgBack"], [10,18],null,null);      
    }
	VerifyAccountsDashBoard();
}

function cancelEnterAmount(){
  kony.automation.playback.wait(3000);
  let currFormName = kony.automation.getCurrentForm();	
  kony.automation.button.click([currFormName,"MakeATransfer","btnAmountCancel"]);
}

function getIndexFromSegment(segData , val){
	for(let i=0; i < segData.length; i++){
		if(segData[i].property === val){
			return i;
		}
	}
	return 0; //default
}

function getBtnID(num){
  switch(num){
    case '0' :
      return "btnZero";
    case '1' :
      return "btnOne";
    case '2' :
      return "btnTwo";
    case '3' :
      return "btnThree";
    case '4' :
      return "btnFour";
    case '5' :
      return "btnFive";
    case '6' :
      return "btnSix";
    case '7' :
      return "btnSeven";
    case '8' :
      return "btnEight";
    case '9' :
      return "btnNine";
  }
}

function getIndexOfAccount_UTF(segData , accountName){
  let i=0;
  let j=0;
  kony.print("segData : "+JSON.stringify(segData));
  for(i=0; i<segData.length; i++){
      if(segData[i].length > 1){
          for(j=0; j<segData[i][1].length; j++){

              if(segData[i][1][j].lblField1.text.includes(accountName)){
                  return { i, j};
              }
          }
      }
  }
  return {i,j };
}