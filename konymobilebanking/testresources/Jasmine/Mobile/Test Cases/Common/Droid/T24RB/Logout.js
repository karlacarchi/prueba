function logout() {
	kony.automation.playback.waitFor(["frmUnifiedDashboard","customHeader","flxBack"],15000);
	kony.automation.flexcontainer.click(["frmUnifiedDashboard","customHeader","flxBack"]);
	kony.automation.playback.waitFor(["frmUnifiedDashboard","Hamburger","flxLogout"],15000);
	kony.automation.widget.touch(["frmUnifiedDashboard","Hamburger","flxLogout"], null,null,[37,31]);
	kony.automation.playback.waitFor(["frmLogout","btnLogIn"],15000);
	kony.automation.button.click(["frmLogout","btnLogIn"]);
	kony.automation.playback.waitFor(["frmLogin","login","tbxUsername"],15000);
}