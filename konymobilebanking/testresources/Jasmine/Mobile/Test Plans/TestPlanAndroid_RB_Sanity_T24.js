require(["Test Suites/Droid/T24RB/PreLoginSupport"], function() {
	require(["Test Suites/Droid/T24RB/PreLoginLanguageChange"], function() {
		require(["Test Suites/Droid/T24RB/Login"], function() {
			require(["Test Suites/Droid/T24RB/Sanity"], function() {
				require(["Test Suites/Droid/RB/RB_Logout"], function() {
										jasmine.getEnv().execute();
				});
			});
		});
	});
});