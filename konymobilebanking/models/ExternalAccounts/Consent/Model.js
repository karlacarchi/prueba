/*
    This is an auto generated file and any modifications to it may result in corrupted data.
*/
define([], function() {
    var BaseModel = kony.mvc.Data.BaseModel;
    var preProcessorCallback;
    var postProcessorCallback;
    var objectMetadata;
    var context = {"object" : "Consent", "objectService" : "ExternalAccounts"};

    var setterFunctions = {
        connect_url: function(val, state) {
            context["field"] = "connect_url";
            context["metadata"] = (objectMetadata ? objectMetadata["connect_url"] : null);
            state['connect_url'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        digitalProfileId: function(val, state) {
            context["field"] = "digitalProfileId";
            context["metadata"] = (objectMetadata ? objectMetadata["digitalProfileId"] : null);
            state['digitalProfileId'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        expires_at: function(val, state) {
            context["field"] = "expires_at";
            context["metadata"] = (objectMetadata ? objectMetadata["expires_at"] : null);
            state['expires_at'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        fetch_scopes: function(val, state) {
            context["field"] = "fetch_scopes";
            context["metadata"] = (objectMetadata ? objectMetadata["fetch_scopes"] : null);
            state['fetch_scopes'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        from_date: function(val, state) {
            context["field"] = "from_date";
            context["metadata"] = (objectMetadata ? objectMetadata["from_date"] : null);
            state['from_date'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        javascript_callback_type: function(val, state) {
            context["field"] = "javascript_callback_type";
            context["metadata"] = (objectMetadata ? objectMetadata["javascript_callback_type"] : null);
            state['javascript_callback_type'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        period_days: function(val, state) {
            context["field"] = "period_days";
            context["metadata"] = (objectMetadata ? objectMetadata["period_days"] : null);
            state['period_days'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        providerCode: function(val, state) {
            context["field"] = "providerCode";
            context["metadata"] = (objectMetadata ? objectMetadata["providerCode"] : null);
            state['providerCode'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
        scopes: function(val, state) {
            context["field"] = "scopes";
            context["metadata"] = (objectMetadata ? objectMetadata["scopes"] : null);
            state['scopes'] = kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, val, context);
        },
    };

    //Create the Model Class
    function Consent(defaultValues) {
        var privateState = {};
        context["field"] = "connect_url";
        context["metadata"] = (objectMetadata ? objectMetadata["connect_url"] : null);
        privateState.connect_url = defaultValues ?
            (defaultValues["connect_url"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["connect_url"], context) :
                null) :
            null;

        context["field"] = "digitalProfileId";
        context["metadata"] = (objectMetadata ? objectMetadata["digitalProfileId"] : null);
        privateState.digitalProfileId = defaultValues ?
            (defaultValues["digitalProfileId"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["digitalProfileId"], context) :
                null) :
            null;

        context["field"] = "expires_at";
        context["metadata"] = (objectMetadata ? objectMetadata["expires_at"] : null);
        privateState.expires_at = defaultValues ?
            (defaultValues["expires_at"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["expires_at"], context) :
                null) :
            null;

        context["field"] = "fetch_scopes";
        context["metadata"] = (objectMetadata ? objectMetadata["fetch_scopes"] : null);
        privateState.fetch_scopes = defaultValues ?
            (defaultValues["fetch_scopes"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["fetch_scopes"], context) :
                null) :
            null;

        context["field"] = "from_date";
        context["metadata"] = (objectMetadata ? objectMetadata["from_date"] : null);
        privateState.from_date = defaultValues ?
            (defaultValues["from_date"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["from_date"], context) :
                null) :
            null;

        context["field"] = "javascript_callback_type";
        context["metadata"] = (objectMetadata ? objectMetadata["javascript_callback_type"] : null);
        privateState.javascript_callback_type = defaultValues ?
            (defaultValues["javascript_callback_type"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["javascript_callback_type"], context) :
                null) :
            null;

        context["field"] = "period_days";
        context["metadata"] = (objectMetadata ? objectMetadata["period_days"] : null);
        privateState.period_days = defaultValues ?
            (defaultValues["period_days"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["period_days"], context) :
                null) :
            null;

        context["field"] = "providerCode";
        context["metadata"] = (objectMetadata ? objectMetadata["providerCode"] : null);
        privateState.providerCode = defaultValues ?
            (defaultValues["providerCode"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["providerCode"], context) :
                null) :
            null;

        context["field"] = "scopes";
        context["metadata"] = (objectMetadata ? objectMetadata["scopes"] : null);
        privateState.scopes = defaultValues ?
            (defaultValues["scopes"] ?
                kony.mvc.util.ProcessorUtils.applyFunction(preProcessorCallback, defaultValues["scopes"], context) :
                null) :
            null;


        //Using parent constructor to create other properties req. to kony sdk
        BaseModel.call(this);

        //Defining Getter/Setters
        Object.defineProperties(this, {
            "connect_url": {
                get: function() {
                    context["field"] = "connect_url";
                    context["metadata"] = (objectMetadata ? objectMetadata["connect_url"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.connect_url, context);
                },
                set: function(val) {
                    setterFunctions['connect_url'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "digitalProfileId": {
                get: function() {
                    context["field"] = "digitalProfileId";
                    context["metadata"] = (objectMetadata ? objectMetadata["digitalProfileId"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.digitalProfileId, context);
                },
                set: function(val) {
                    setterFunctions['digitalProfileId'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "expires_at": {
                get: function() {
                    context["field"] = "expires_at";
                    context["metadata"] = (objectMetadata ? objectMetadata["expires_at"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.expires_at, context);
                },
                set: function(val) {
                    setterFunctions['expires_at'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "fetch_scopes": {
                get: function() {
                    context["field"] = "fetch_scopes";
                    context["metadata"] = (objectMetadata ? objectMetadata["fetch_scopes"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.fetch_scopes, context);
                },
                set: function(val) {
                    setterFunctions['fetch_scopes'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "from_date": {
                get: function() {
                    context["field"] = "from_date";
                    context["metadata"] = (objectMetadata ? objectMetadata["from_date"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.from_date, context);
                },
                set: function(val) {
                    setterFunctions['from_date'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "javascript_callback_type": {
                get: function() {
                    context["field"] = "javascript_callback_type";
                    context["metadata"] = (objectMetadata ? objectMetadata["javascript_callback_type"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.javascript_callback_type, context);
                },
                set: function(val) {
                    setterFunctions['javascript_callback_type'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "period_days": {
                get: function() {
                    context["field"] = "period_days";
                    context["metadata"] = (objectMetadata ? objectMetadata["period_days"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.period_days, context);
                },
                set: function(val) {
                    setterFunctions['period_days'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "providerCode": {
                get: function() {
                    context["field"] = "providerCode";
                    context["metadata"] = (objectMetadata ? objectMetadata["providerCode"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.providerCode, context);
                },
                set: function(val) {
                    setterFunctions['providerCode'].call(this, val, privateState);
                },
                enumerable: true,
            },
            "scopes": {
                get: function() {
                    context["field"] = "scopes";
                    context["metadata"] = (objectMetadata ? objectMetadata["scopes"] : null);
                    return kony.mvc.util.ProcessorUtils.applyFunction(postProcessorCallback, privateState.scopes, context);
                },
                set: function(val) {
                    setterFunctions['scopes'].call(this, val, privateState);
                },
                enumerable: true,
            },
        });

        //converts model object to json object.
        this.toJsonInternal = function() {
            return Object.assign({}, privateState);
        };

        //overwrites object state with provided json value in argument.
        this.fromJsonInternal = function(value) {
            privateState.connect_url = value ? (value["connect_url"] ? value["connect_url"] : null) : null;
            privateState.digitalProfileId = value ? (value["digitalProfileId"] ? value["digitalProfileId"] : null) : null;
            privateState.expires_at = value ? (value["expires_at"] ? value["expires_at"] : null) : null;
            privateState.fetch_scopes = value ? (value["fetch_scopes"] ? value["fetch_scopes"] : null) : null;
            privateState.from_date = value ? (value["from_date"] ? value["from_date"] : null) : null;
            privateState.javascript_callback_type = value ? (value["javascript_callback_type"] ? value["javascript_callback_type"] : null) : null;
            privateState.period_days = value ? (value["period_days"] ? value["period_days"] : null) : null;
            privateState.providerCode = value ? (value["providerCode"] ? value["providerCode"] : null) : null;
            privateState.scopes = value ? (value["scopes"] ? value["scopes"] : null) : null;
        };
    }

    //Setting BaseModel as Parent to this Model
    BaseModel.isParentOf(Consent);

    //Create new class level validator object
    BaseModel.Validator.call(Consent);

    var registerValidatorBackup = Consent.registerValidator;

    Consent.registerValidator = function() {
        var propName = arguments[0];
        if(!setterFunctions[propName].changed) {
            var setterBackup = setterFunctions[propName];
            setterFunctions[arguments[0]] = function() {
                if(Consent.isValid(this, propName, val)) {
                    return setterBackup.apply(null, arguments);
                } else {
                    throw Error("Validation failed for " + propName + " : " + val);
                }
            }
            setterFunctions[arguments[0]].changed = true;
        }
        return registerValidatorBackup.apply(null, arguments);
    }

    //Extending Model for custom operations
    //For Operation 'createTermsAndConditions' with service id 'createTermsAndConditions7692'
     Consent.createTermsAndConditions = function(params, onCompletion){
        return Consent.customVerb('createTermsAndConditions', params, onCompletion);
     };

    //For Operation 'refreshConsent' with service id 'refreshConnection4725'
     Consent.refreshConsent = function(params, onCompletion){
        return Consent.customVerb('refreshConsent', params, onCompletion);
     };

    var relations = [];

    Consent.relations = relations;

    Consent.prototype.isValid = function() {
        return Consent.isValid(this);
    };

    Consent.prototype.objModelName = "Consent";

    /*This API allows registration of preprocessors and postprocessors for model.
     *It also fetches object metadata for object.
     *Options Supported
     *preProcessor  - preprocessor function for use with setters.
     *postProcessor - post processor callback for use with getters.
     *getFromServer - value set to true will fetch metadata from network else from cache.
     */
    Consent.registerProcessors = function(options, successCallback, failureCallback) {

        if(!options) {
            options = {};
        }

        if(options && ((options["preProcessor"] && typeof(options["preProcessor"]) === "function") || !options["preProcessor"])) {
            preProcessorCallback = options["preProcessor"];
        }

        if(options && ((options["postProcessor"] && typeof(options["postProcessor"]) === "function") || !options["postProcessor"])) {
            postProcessorCallback = options["postProcessor"];
        }

        function metaDataSuccess(res) {
            objectMetadata = kony.mvc.util.ProcessorUtils.convertObjectMetadataToFieldMetadataMap(res);
            successCallback();
        }

        function metaDataFailure(err) {
            failureCallback(err);
        }

        kony.mvc.util.ProcessorUtils.getMetadataForObject("ExternalAccounts", "Consent", options, metaDataSuccess, metaDataFailure);
    };

    //clone the object provided in argument.
    Consent.clone = function(objectToClone) {
        var clonedObj = new Consent();
        clonedObj.fromJsonInternal(objectToClone.toJsonInternal());
        return clonedObj;
    };

    return Consent;
});